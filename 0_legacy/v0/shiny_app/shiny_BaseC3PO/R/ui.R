# Define UI for application that draws a histogram

ui <- fluidPage(
  theme = shinytheme("slate"),
  br(),
  sidebarLayout(
    sidebarPanel(
      width = 2,
      selectInput(
        "select_substance", label = h4("Substance"), 
        choices = ui_BNVD_substance, 
        selected = ui_BNVD_substance[1])
    ),
    mainPanel(
      navbarPage(
        "Base C3PO",
        # BNVD
        tabPanel(
          "BNVD",
          plotlyOutput('BNVD_plt_sa'),
          br(),
          sidebarLayout(
            sidebarPanel(
             width = 3,
             selectInput(
               "select_annee", label = h4("Année"), 
               choices = ui_BNVD_annee, 
               selected = ui_BNVD_annee[1]),
             sliderInput("BNVD_nbr_sa", label = h4("Nombre de substances"),
                         min = 1, max = length(ui_BNVD_substance),
                         value = 50, step = 1)
            ),
            mainPanel(
              plotlyOutput('BNVD_plt_yr'),
            )
          )
        ),
        # AGRITOX
        navbarMenu(
          "AGRITOX",
          tabPanel(
            "Toxicité",
            DT::DTOutput('AGRI_tox_tab'),
            br(),
            tags$hr(style="border-color: white;"),
            sidebarLayout(
              sidebarPanel(
                selectInput("AGRItox_spe",
                            label = h4("Espèces"), 
                            choices = c('ALL', AGRItox_species), 
                            selected = AGRItox_species[1]),
                selectInput("AGRItox_etu",
                            label = h4("Etudes"), 
                            choices = c('ALL', AGRItox_etude), 
                            selected = AGRItox_etude[1]),
                selectInput("AGRItox_mes",
                            label = h4("Mesures"), 
                            choices = c('ALL', AGRItox_mesure), 
                            selected = AGRItox_mesure[1])
              ),
              mainPanel(
                plotlyOutput('AGRI_tox_plt')
              )
            )
          ),
          tabPanel(
            "Ecotoxicité",
            DT::DTOutput('AGRI_ecotox_tab'),
            br(),
            tags$hr(style="border-color: white;"),
            sidebarLayout(
              sidebarPanel(
                selectInput("AGRIecotox_etu",
                            label = h4("Etudes"), 
                            choices = AGRIecotox_etude, 
                            selected = AGRIecotox_etude[1]),
                selectInput("AGRIecotox_mes",
                            label = h4("Mesures"), 
                            choices = AGRIecotox_mesure, 
                            selected = AGRIecotox_mesure[1])
              ),
              mainPanel(
                plotlyOutput('AGRI_ecotox_plt')
              )
            )
          ),
          # VTR
          tabPanel(
            "VTR",
            DT::DTOutput('AGRI_VTR_tab'),
            br(),
            tags$hr(style="border-color: white;"),
            sidebarLayout(
              sidebarPanel(
                selectInput("AGRIvtr_etu",
                            label = h4("Etudes"), 
                            choices = AGRIvtr_etude, 
                            selected = AGRIvtr_etude[1]),
                selectInput("AGRIvtr_mes",
                            label = h4("Mesures"), 
                            choices = AGRIvtr_mesure, 
                            selected = AGRIvtr_mesure[1])
              ),
              mainPanel(
                plotlyOutput('AGRI_vtr_plt')
              )
            )
          )
        ),
        # EU PDB
        tabPanel(
          "EU_PDB",
          h3("Pays autorisant la substance"),
          leafletOutput('EU_map'),
          br(),
          tags$hr(style="border-color: white;"),
          sidebarLayout(
            sidebarPanel(
              width = 3,
              sliderInput("EU_sa",
                          label = h4("Substances"),
                          min = 1, max = nrow(EU_DF),
                          value = c(1,20)),
              p("La sélection se fait de la substance la plus vendue à la moins vendus
                sur l'ensemble des années de la BNV-D.")
            ),
            mainPanel(
                plotlyOutput('EU_plt')
            )
          )
        )
      )
    )
  )
)

