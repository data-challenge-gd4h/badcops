# C3PO API

L'API C3BO se base sur les données de la BNV-D, enrichies par différentes sources Open Data sur les produits phytosanitaires et les substances actives.

Pour en savoir plus sur les données consolidées, vous pouvez accéder à https://gitlab.com/data-challenge-gd4h/badcops


## Authors

- [@agrig33k](https://www.github.com/agrig33k)


## Roadmap

V1.0.0

- Finaliser les schémas "Substance" et "Produit"
- Intégrer les données en base de données
- Développement de l'API 
- Déploiement de la v1.0.0

V1.0.1

- Ajout des points d'entrée référentiels
- Ajout des schémas correspondants
- Ajout des filtres supplémentaires sur les substances et produits
