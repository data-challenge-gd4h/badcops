CREATE TABLE "public.id_bnvd_substance" (
	"substance" TEXT NOT NULL,
	"cas" TEXT,
	CONSTRAINT "id_bnvd_substance_pk" PRIMARY KEY ("substance","cas")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.id_agritox_x_id_bnv-d" (
	"cas_agritox" TEXT,
	"new_field" TEXT,
	"cas_bnvd" TEXT,
	"substance_bnvd" TEXT NOT NULL,
	CONSTRAINT "id_agritox_x_id_bnv-d_pk" PRIMARY KEY ("cas_agritox","new_field")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.id_atp_x_id_bnvd" (
	"cas_bnvd" TEXT NOT NULL,
	"substance_bnvd" TEXT NOT NULL,
	"ici_atp" TEXT,
	"cas_atp" TEXT,
	"index_atp" TEXT,
	CONSTRAINT "id_atp_x_id_bnvd_pk" PRIMARY KEY ("ici_atp","cas_atp","index_atp")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.id_ephy_x_id_bnvd" (
	"substance_bnvd" TEXT NOT NULL,
	"cas_bnvd" TEXT,
	"substance_ephy" TEXT NOT NULL,
	"cas_ephy" TEXT,
	"variant_ephy" TEXT,
	"cas_variant_ephy" TEXT,
	CONSTRAINT "id_ephy_x_id_bnvd_pk" PRIMARY KEY ("substance_ephy","cas_variant_ephy")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.infos_agritox" (
	"cas_agritox" TEXT NOT NULL,
	"substance_agritox" TEXT NOT NULL,
	"nom_uipac" TEXT,
	"ec" TEXT,
	"masse_molaire" double,
	"formule_brute" TEXT,
	"purete_minimale" TEXT,
	"impuretes_pertinentes" TEXT,
	"classement_ref" TEXT,
	"classement_date" DATE,
	"classement_code_h" TEXT,
	"classement_categorie_danger" TEXT,
	"classement_mention_danger" TEXT,
	"classification_simpl" TEXT,
	"classement_cmr1" integer,
	"classement_cmr2" integer,
	"classement_allaitement" integer,
	"classement_tox_humaine" integer,
	"classement_env_a" integer,
	"classement_env_b" integer,
	"classement_autre_sante" integer,
	"classement_autre_environnement" integer,
	"classement_autre_explosif" integer,
	"classement_autre_inflammable" integer,
	"classement_autre_corrosif" integer,
	"classement_autre" integer,
	"classement_sans_classement" integer,
	"pc_propriete" TEXT,
	"pc_valeur" TEXT,
	"pc_unite" TEXT,
	"pc_temperature" TEXT,
	"pc_ph" TEXT,
	"pc_observation" TEXT,
	"toxicite_nature_etude" TEXT,
	"toxicite_dl50_cl50" TEXT,
	"toxicite_valeur" TEXT,
	"toxicite_unite" TEXT,
	"toxicite_espece" TEXT,
	"toxicite_sexe" TEXT,
	"ecotox_valeur_pnec" TEXT,
	"ecotox_unite_pnec" TEXT,
	"ecotox_etudes" TEXT,
	"ecotox_donnee_toxicite" TEXT,
	"ecotox_donnee_toxicite_valeur" TEXT,
	"ecotox_donnee_toxicite_unite" TEXT,
	"ecotox_facteur_securite" TEXT,
	"vtr_source" TEXT,
	"vtr_date" DATE,
	"vtr_nom" TEXT,
	"vtr_valeur" TEXT,
	"vtr_unite" TEXT,
	"vtr_etude_pivot_1" TEXT,
	"vtr_etude_pivot_1_noael" TEXT,
	"vtr_etude_pivot_1_noael_unite" TEXT,
	"vtr_etude_pivot_2" TEXT,
	"vtr_facteur_securite" TEXT,
	"vtr_facteur_correction_abs_orale_incompl" TEXT,
	CONSTRAINT "infos_agritox_pk" PRIMARY KEY ("cas_agritox","substance_agritox")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.infos_atp" (
	"ici_atp" TEXT,
	"cas_atp" TEXT,
	"index_atp" TEXT,
	"ec_atp" TEXT,
	"classement_code_h_detail" TEXT,
	"classement_code_h" TEXT,
	"classement_categorie_danger" TEXT,
	"classement_mention_danger" TEXT,
	"classement_cmr1" BOOLEAN,
	"classement_cmr2" BOOLEAN,
	"classement_allaitement" BOOLEAN,
	"classement_tox_humaine" BOOLEAN,
	"classement_env_a" BOOLEAN,
	"classement_env_b" BOOLEAN,
	"classement_autre_sante" BOOLEAN,
	"classement_autre_environnement" BOOLEAN,
	"classement_autre_explosif" BOOLEAN,
	"classement_autre_inflammable" BOOLEAN,
	"classement_autre_corrosif" BOOLEAN,
	"classement_autre" BOOLEAN,
	"classement_sans_classement" BOOLEAN,
	CONSTRAINT "infos_atp_pk" PRIMARY KEY ("ici_atp","cas_atp","index_atp")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.infos_bnvd_substance" (
	"substance_bnvd" TEXT NOT NULL,
	"cas_bnvd" TEXT,
	"fonction_principale" TEXT NOT NULL,
	"fonction_precision" TEXT,
	"fonction_autres" TEXT,
	"annee" integer NOT NULL,
	"annee_min" integer NOT NULL,
	"annee_max" integer NOT NULL,
	"rpd_classification" TEXT,
	"rpd_mention" TEXT,
	"rpd_classification_last" TEXT,
	"rpd_mention_last" TEXT,
	"rpd_classification_taux" TEXT,
	"rpd_mention_taux" TEXT,
	"rpd_taux" TEXT,
	"rpd_taux_last" TEXT,
	CONSTRAINT "infos_bnvd_substance_pk" PRIMARY KEY ("substance_bnvd","cas_bnvd")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.infos_ephy" (
	"substance_ephy" TEXT NOT NULL,
	"cas_ephy" TEXT NOT NULL,
	"variant_ephy" TEXT NOT NULL,
	"cas_variant_ephy" TEXT NOT NULL,
	"etat_autorisation" TEXT NOT NULL
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.id_eu_pdb_x_id_bnv-d" (
	"substance_id_eupbd" TEXT NOT NULL,
	"substance_eupbd" TEXT,
	"substance_bnvd" TEXT NOT NULL,
	"cas_bnvd" TEXT,
	CONSTRAINT "id_eu_pdb_x_id_bnv-d_pk" PRIMARY KEY ("substance_id_eupbd")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.id_sandre_x_id_bnv-d" (
	"cdparametre" TEXT,
	"nomparametre" TEXT,
	"cas_bnvd" TEXT,
	"substance_bnvd" TEXT,
	CONSTRAINT "id_sandre_x_id_bnv-d_pk" PRIMARY KEY ("cdparametre","nomparametre","cas_bnvd","substance_bnvd")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.infos_sandre" (
	"cdparametre" TEXT,
	"nomparametre" TEXT,
	"cdgroupeparametres" TEXT,
	"nomgroupeparametres" TEXT,
	"famille" TEXT,
	"fonction" TEXT
) WITH (
  OIDS=FALSE
);

CREATE TABLE "public.infos_eu_pdb" (
	"substance_id_eupbd" TEXT,
	"substance_eupbd" TEXT,
	"status_reg_1107_2019" integer,
	"date_approbation" DATE,
	"date_expiration_approbation" DATE,
	"candidat_substitution" TEXT,
	"substance_basique" TEXT,
	"risque_bas" TEXT,
	"autorisations_pays" TEXT,
	"autorisation_france" TEXT
	CONSTRAINT "infos_eu_pdb_pk" PRIMARY KEY ("substance_id_eupbd","substance_eupbd")
) WITH (
  OIDS=FALSE
);



ALTER TABLE "id_agritox_x_id_bnv-d" ADD CONSTRAINT "id_agritox_x_id_bnv-d_fk0" FOREIGN KEY ("cas_bnvd") REFERENCES "id_bnvd_substance"("cas");
ALTER TABLE "id_agritox_x_id_bnv-d" ADD CONSTRAINT "id_agritox_x_id_bnv-d_fk1" FOREIGN KEY ("substance_bnvd") REFERENCES "id_bnvd_substance"("substance");

ALTER TABLE "id_atp_x_id_bnvd" ADD CONSTRAINT "id_atp_x_id_bnvd_fk0" FOREIGN KEY ("cas_bnvd") REFERENCES "id_bnvd_substance"("cas");
ALTER TABLE "id_atp_x_id_bnvd" ADD CONSTRAINT "id_atp_x_id_bnvd_fk1" FOREIGN KEY ("substance_bnvd") REFERENCES "id_bnvd_substance"("substance");

ALTER TABLE "id_ephy_x_id_bnvd" ADD CONSTRAINT "id_ephy_x_id_bnvd_fk0" FOREIGN KEY ("substance_bnvd") REFERENCES "id_bnvd_substance"("substance");
ALTER TABLE "id_ephy_x_id_bnvd" ADD CONSTRAINT "id_ephy_x_id_bnvd_fk1" FOREIGN KEY ("cas_bnvd") REFERENCES "id_bnvd_substance"("cas");

ALTER TABLE "infos_agritox" ADD CONSTRAINT "infos_agritox_fk0" FOREIGN KEY ("cas_agritox") REFERENCES "id_agritox_x_id_bnv-d"("cas_agritox");
ALTER TABLE "infos_agritox" ADD CONSTRAINT "infos_agritox_fk1" FOREIGN KEY ("substance_agritox") REFERENCES "id_agritox_x_id_bnv-d"("cas_agritox");

ALTER TABLE "infos_atp" ADD CONSTRAINT "infos_atp_fk0" FOREIGN KEY ("ici_atp") REFERENCES "id_atp_x_id_bnvd"("ici_atp");
ALTER TABLE "infos_atp" ADD CONSTRAINT "infos_atp_fk1" FOREIGN KEY ("cas_atp") REFERENCES "id_atp_x_id_bnvd"("cas_atp");
ALTER TABLE "infos_atp" ADD CONSTRAINT "infos_atp_fk2" FOREIGN KEY ("index_atp") REFERENCES "id_atp_x_id_bnvd"("index_atp");

ALTER TABLE "infos_bnvd_substance" ADD CONSTRAINT "infos_bnvd_substance_fk0" FOREIGN KEY ("substance_bnvd") REFERENCES "id_bnvd_substance"("substance");
ALTER TABLE "infos_bnvd_substance" ADD CONSTRAINT "infos_bnvd_substance_fk1" FOREIGN KEY ("cas_bnvd") REFERENCES "id_bnvd_substance"("cas");

ALTER TABLE "infos_ephy" ADD CONSTRAINT "infos_ephy_fk0" FOREIGN KEY ("substance_ephy") REFERENCES "id_ephy_x_id_bnvd"("substance_ephy");
ALTER TABLE "infos_ephy" ADD CONSTRAINT "infos_ephy_fk1" FOREIGN KEY ("cas_ephy") REFERENCES "id_ephy_x_id_bnvd"("cas_ephy");
ALTER TABLE "infos_ephy" ADD CONSTRAINT "infos_ephy_fk2" FOREIGN KEY ("variant_ephy") REFERENCES "id_ephy_x_id_bnvd"("variant_ephy");
ALTER TABLE "infos_ephy" ADD CONSTRAINT "infos_ephy_fk3" FOREIGN KEY ("cas_variant_ephy") REFERENCES "id_ephy_x_id_bnvd"("cas_variant_ephy");

ALTER TABLE "id_eu_pdb_x_id_bnv-d" ADD CONSTRAINT "id_eu_pdb_x_id_bnv-d_fk0" FOREIGN KEY ("substance_bnvd") REFERENCES "id_bnvd_substance"("substance");
ALTER TABLE "id_eu_pdb_x_id_bnv-d" ADD CONSTRAINT "id_eu_pdb_x_id_bnv-d_fk1" FOREIGN KEY ("cas_bnvd") REFERENCES "id_bnvd_substance"("cas");

ALTER TABLE "id_sandre_x_id_bnv-d" ADD CONSTRAINT "id_sandre_x_id_bnv-d_fk0" FOREIGN KEY ("cas_bnvd") REFERENCES "id_bnvd_substance"("cas");
ALTER TABLE "id_sandre_x_id_bnv-d" ADD CONSTRAINT "id_sandre_x_id_bnv-d_fk1" FOREIGN KEY ("substance_bnvd") REFERENCES "id_bnvd_substance"("substance");

ALTER TABLE "infos_sandre" ADD CONSTRAINT "infos_sandre_fk0" FOREIGN KEY ("cdparametre") REFERENCES "id_sandre_x_id_bnv-d"("cdparametre");
ALTER TABLE "infos_sandre" ADD CONSTRAINT "infos_sandre_fk1" FOREIGN KEY ("nomparametre") REFERENCES "id_sandre_x_id_bnv-d"("nomparametre");


ALTER TABLE "infos_eu_pdb" ADD CONSTRAINT "infos_eu_pdb_fk0" FOREIGN KEY ("substance_id_eupbd") REFERENCES "id_eu_pdb_x_id_bnv-d"("substance_id_eupbd");
ALTER TABLE "infos_eu_pdb" ADD CONSTRAINT "infos_eu_pdb_fk1" FOREIGN KEY ("substance_eupbd") REFERENCES "id_eu_pdb_x_id_bnv-d"("substance_eupbd");









