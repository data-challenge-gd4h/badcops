#!/bin/bash

echo "Bienvenue dans le script de restauration du dump dans la base de données C3PO."

read -p "Entrez le répertoire bin de postgresql (par exemple /c/Program Files/PostgreSQL/14/bin): " repertoire_psql
read -p "Entrez l'hôte (par exemple localhost) : " hote
read -p "Entrez le port (par exemple 5432) : " port
read -p "Entrez le nom d'utilisateur (par exemple postgres) : " utilisateur
read -s -p "Entrez le mot de passe : " mot_de_passe

# Ajouter le répertoire bin de PostgreSQL à la variable PATH
export PATH="$repertoire_psql:$PATH"
echo

# Récupérer le chemin du répertoire actuel du script
repertoire_script=$(dirname "$0")

# Chemin complet vers le fichier de sauvegarde
chemin_dump="$repertoire_script/c3po_dump.sql"

# Vérifier si la base de données c3po existe
if PGPASSWORD="$mot_de_passe" psql -h "$hote" -p "$port" -U "$utilisateur" -lqt | cut -d \| -f 1 | grep -qw c3po; then
	read -p "La base de données c3po existe déjà. Voulez-vous la supprimer et restaurer le dump ? (Oui/Non): " reponse
    if [ "$reponse" = "Oui" ]; then
        # Supprimer la base de données c3po existante
		PGPASSWORD="$mot_de_passe" dropdb -h "$hote" -p "$port" -U "$utilisateur" c3po
	else
		echo "Opération annulée. Fermeture de la console."
		# Ajouter une pause pour empêcher la fermeture automatique
		read -p "Appuyez sur Entrée pour quitter..."
		exit 0
	fi
fi

# Créer la base de données c3po
PGPASSWORD="$mot_de_passe" createdb -h "$hote" -p "$port" -U "$utilisateur" -E UTF8 -T template0 c3po

# Utiliser la commande psql pour restaurer le dump
PGPASSWORD="$mot_de_passe" psql -h "$hote" -p "$port" -U "$utilisateur" -d c3po -f "$chemin_dump"

echo "La restauration du dump dans la base de données C3PO est terminée."

# Ajouter une pause pour empêcher la fermeture automatique
read -p "Appuyez sur Entrée pour quitter..."