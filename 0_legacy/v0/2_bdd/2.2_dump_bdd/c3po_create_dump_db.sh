#!/bin/bash

echo "Bienvenue dans le script de sauvegarde de la base de données C3PO."

read -p "Entrez le répertoire bin de postgresql (par exemple /c/Program Files/PostgreSQL/14/bin): " repertoire_psql
read -p "Entrez l'hôte (par exemple localhost) : " hote
read -p "Entrez le port (par exemple 5432) : " port
read -p "Entrez le nom d'utilisateur (par exemple postgres) : " utilisateur
read -s -p "Entrez le mot de passe : " mot_de_passe

# Ajouter le répertoire bin de PostgreSQL à la variable PATH
export PATH="$repertoire_psql:$PATH"
echo

# Récupérer le chemin du répertoire actuel du script
repertoire_script=$(dirname "$0")

# Définir le chemin de sauvegarde du dump
chemin_sauvegarde="$repertoire_script\c3po_dump.sql"

# Utiliser pg_dump pour créer le dump de la base de données
PGPASSWORD="$mot_de_passe" pg_dump -h "$hote" -p "$port" -U "$utilisateur" -d c3po -f "$chemin_sauvegarde"

echo "Le dump de la base de données C3PO a été créé dans : $chemin_sauvegarde"

# Ajouter une pause pour empêcher la fermeture automatique
read -p "Appuyez sur Entrée pour quitter..."
