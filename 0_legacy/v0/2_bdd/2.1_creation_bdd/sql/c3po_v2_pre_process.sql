---
--- Création de la base de données
---
DROP DATABASE IF EXISTS c3po;
CREATE DATABASE c3po;

---
--- Utilisation de la base de données
---
\c c3po

---
--- Création des schémas
---
DROP SCHEMA IF EXISTS amm CASCADE;
DROP SCHEMA IF EXISTS substance CASCADE;

CREATE SCHEMA amm;
CREATE SCHEMA substance;

---
--- Création des tables
---
CREATE TABLE amm.id_bnvd_amm (amm_bnvd TEXT NOT NULL,
CONSTRAINT id_bnvd_amm_pk PRIMARY KEY (amm_bnvd)
) WITH (OIDS=FALSE);

CREATE TABLE amm.infos_bnvd_amm (amm_bnvd TEXT NOT NULL,
annee INTEGER NOT NULL,
annee_min INTEGER NOT NULL,
annee_max INTEGER NOT NULL,
conditionnement TEXT NOT NULL,
eaj TEXT NOT NULL,
substance_bnvd TEXT NOT NULL,
cas_bnvd TEXT,
substance_bnvd_cas_bnvd TEXT NOT NULL
) WITH (OIDS=FALSE);

CREATE TABLE amm.id_ephy_x_id_bnvd_amm (amm_bnvd TEXT NOT NULL,
amm_ephy TEXT,
pcp_ephy TEXT
) WITH (OIDS=FALSE);

CREATE TABLE amm.infos_ephy_amm_identite (amm_ephy TEXT,
pcp_ephy TEXT,
nom_produit TEXT,
seconds_noms_produit TEXT,
titulaire TEXT,
type_commercial TEXT,
type_produit TEXT,
amm_etat_autorisation TEXT,
pcp_etat_autorisation TEXT,
amm_date_retrait TIMESTAMP,
amm_date_premiere_autorisation TIMESTAMP,
produit_ref_amm TEXT,
produit_ref_nom TEXT
) WITH (OIDS=FALSE);

CREATE TABLE amm.infos_ephy_amm_composition (amm_ephy TEXT,
pcp_ephy TEXT,
substance_ephy_cas_ephy_variant_ephy TEXT,
substance_ephy TEXT,
variant_ephy_exp TEXT,
cas_ephy TEXT,
teneur_substance REAL,
teneur_substance_unite TEXT
) WITH (OIDS=FALSE);

CREATE TABLE amm.infos_ephy_amm_usages (amm_ephy TEXT,
pcp_ephy TEXT,
usage_gamme TEXT,
mentions_autorisees TEXT,
usage_restrictions_court TEXT,
usage_restrictions_long TEXT,
fonctions TEXT,
formulations TEXT,
usage TEXT,
usage_date_decision TIMESTAMP,
stade_cultural_min_bbch INTEGER,
stade_cultural_max_bbch INTEGER,
usage_etat TEXT,
dose_homologuee REAL,
dose_homologuee_unite TEXT,
dar_jours INTEGER,
dar_bbch INTEGER,
nb_max_applications INTEGER,
usage_date_fin_distribution TIMESTAMP,
usage_date_fin_utilisation TIMESTAMP,
conditions_emploi TEXT,
znt_aquatique INTEGER,
znt_arthropodes_non_cibles INTEGER,
znt_plantes_non_cibles INTEGER,
intervalle_min INTEGER,
cultures_autorisees_ephy TEXT,
cultures_autorisees_rpg TEXT
) WITH (OIDS=FALSE);


CREATE TABLE amm.infos_ephy_amm_mention_danger (amm_ephy TEXT,
pcp_ephy TEXT,
classes_mention_danger_lb_court TEXT,
classes_mention_danger_lb_long TEXT
) WITH (OIDS=FALSE);

CREATE TABLE amm.infos_ephy_amm_phrases_risque (amm_ephy TEXT,
pcp_ephy TEXT,
phrases_risque_lb_court TEXT,
phrases_risque_lb_long TEXT
) WITH (OIDS=FALSE);

CREATE TABLE substance.id_bnvd_sub (substance_bnvd TEXT NOT NULL,
cas_bnvd TEXT,
substance_bnvd_cas_bnvd TEXT NOT NULL,
CONSTRAINT id_bnvd_sub_pk PRIMARY KEY (substance_bnvd_cas_bnvd)
) WITH (OIDS=FALSE);

CREATE TABLE substance.infos_bnvd_sub (substance_bnvd TEXT NOT NULL,
cas_bnvd TEXT,
substance_bnvd_cas_bnvd TEXT NOT NULL,
fonction_principale TEXT NOT NULL,
fonction_precision TEXT,
fonction_autres TEXT,
annee INTEGER NOT NULL,
annee_min INTEGER NOT NULL,
annee_max INTEGER NOT NULL,
rpd_classification TEXT NOT NULL,
rpd_mention TEXT,
rpd_classification_last TEXT NOT NULL,
rpd_mention_last TEXT,
rpd_classification_taux REAL NOT NULL,
rpd_mention_taux REAL,
rpd_taux REAL NOT NULL,
rpd_taux_last REAL NOT NULL
) WITH (OIDS=FALSE);

CREATE TABLE substance.id_ephy_x_id_bnvd_sub (substance_bnvd TEXT NOT NULL,
cas_bnvd TEXT,
substance_bnvd_cas_bnvd TEXT NOT NULL,
substance_ephy TEXT,
cas_ephy TEXT,
variant_ephy TEXT,
variant_ephy_exp TEXT,
cas_variant_ephy TEXT,
substance_ephy_cas_ephy_variant_ephy TEXT
) WITH (OIDS=FALSE);

CREATE TABLE substance.infos_ephy_sub (substance_ephy_cas_ephy_variant_ephy TEXT,
substance_ephy TEXT,
cas_ephy TEXT,
variant_ephy TEXT,
variant_ephy_exp TEXT,
cas_variant_ephy TEXT,
etat_autorisation TEXT
) WITH (OIDS=FALSE);

CREATE TABLE substance.id_agritox_x_id_bnvd_sub (substance_bnvd TEXT NOT NULL,
cas_bnvd TEXT,
substance_bnvd_cas_bnvd TEXT NOT NULL,
substance_agritox TEXT,
cas_agritox TEXT,
substance_agritox_cas_agritox TEXT
) WITH (OIDS=FALSE);

CREATE TABLE substance.infos_agritox_sub (substance_agritox TEXT,
cas_agritox TEXT,
substance_agritox_cas_agritox TEXT,
nom_uipac TEXT,
ec TEXT,
masse_molaire TEXT,
formule_brute TEXT,
purete_minimale TEXT,
impuretes_pertinentes TEXT,
classement_ref TEXT,
classement_date TIMESTAMP,
classement_code_h TEXT,
classement_categorie_danger TEXT,
classement_mention_danger TEXT,
classification_simpl TEXT,
pc_propriete TEXT,
pc_temperature TEXT,
pc_ph TEXT,
pc_observation TEXT,
pc_valeur TEXT,
pc_unite TEXT,
toxicite_nature_etude TEXT,
toxicite_dl50_cl50 TEXT,
toxicite_espece TEXT,
toxicite_sexe TEXT,
toxicite_valeur TEXT,
toxicite_unite TEXT,
ecotox_etudes TEXT,
ecotox_donnee_toxicite TEXT,
ecotox_donnee_toxicite_valeur TEXT,
ecotox_donnee_toxicite_unite TEXT,
ecotox_facteur_securite TEXT,
ecotox_valeur_pnec TEXT,
ecotox_unite_pnec TEXT,
vtr_nom TEXT,
vtr_valeur TEXT,
vtr_unite TEXT,
vtr_source TEXT,
vtr_date TIMESTAMP,
vtr_etude_pivot_1 TEXT,
vtr_etude_pivot_1_noael TEXT,
vtr_etude_pivot_1_noael_unite TEXT,
vtr_etude_pivot_2 TEXT,
vtr_facteur_securite TEXT,
vtr_facteur_correction_abs_orale_incompl TEXT
) WITH (OIDS=FALSE);

CREATE TABLE substance.id_atp_x_id_bnvd_sub (substance_bnvd TEXT NOT NULL,
cas_bnvd TEXT,
substance_bnvd_cas_bnvd TEXT NOT NULL,
index_atp TEXT,
ici_atp TEXT,
ec_atp TEXT,
cas_atp TEXT
) WITH (OIDS=FALSE);

CREATE TABLE substance.infos_atp_sub (index_atp TEXT,
ici_atp TEXT,
ec_atp TEXT,
cas_atp TEXT,
classement_code_h_detail TEXT,
classement_code_h TEXT,
classement_categorie_danger TEXT,
classement_mention_danger TEXT,
classification_simpl TEXT
) WITH (OIDS=FALSE);

CREATE TABLE substance.id_eupdb_x_id_bnvd_sub (substance_bnvd TEXT NOT NULL,
cas_bnvd TEXT,
substance_bnvd_cas_bnvd TEXT NOT NULL,
substance_id_eupbd TEXT,
substance_eupbd TEXT
) WITH (OIDS=FALSE);

CREATE TABLE substance.infos_eupdb_sub (substance_id_eupbd TEXT,
substance_eupbd TEXT,
etat_reg_1107_2019 TEXT,
date_approbation TIMESTAMP,
date_expiration_approbation TIMESTAMP,
substance_base TEXT,
candidat_substitution TEXT,
faible_risque TEXT,
autorisations_pays TEXT,
autorisation_france TEXT
) WITH (OIDS=FALSE);

CREATE TABLE substance.id_sandre_x_id_bnvd_sub (substance_bnvd TEXT NOT NULL,
cas_bnvd TEXT,
substance_bnvd_cas_bnvd TEXT NOT NULL,
cdparametre TEXT,
nomparametre TEXT
) WITH (OIDS=FALSE);

CREATE TABLE substance.infos_sandre_sub (cdparametre TEXT,
nomparametre TEXT,
cdgroupeparametres TEXT,
nomgroupeparametres TEXT,
famille TEXT,
fonction TEXT
) WITH (OIDS=FALSE);

---
--- Ajout des clés étrangères
---
ALTER TABLE amm.id_bnvd_amm ADD CONSTRAINT id_bnvd_amm_fk0 FOREIGN KEY (amm_bnvd) REFERENCES amm.id_bnvd_amm (amm_bnvd);

ALTER TABLE amm.infos_bnvd_amm ADD CONSTRAINT infos_bnvd_amm_fk0 FOREIGN KEY (amm_bnvd) REFERENCES amm.id_bnvd_amm (amm_bnvd);
ALTER TABLE amm.infos_bnvd_amm ADD CONSTRAINT infos_bnvd_amm_fk1 FOREIGN KEY (substance_bnvd_cas_bnvd) REFERENCES substance.id_bnvd_sub (substance_bnvd_cas_bnvd);

ALTER TABLE amm.id_ephy_x_id_bnvd_amm ADD CONSTRAINT id_ephy_x_id_bnvd_amm_fk0 FOREIGN KEY (amm_bnvd) REFERENCES amm.id_bnvd_amm (amm_bnvd);

ALTER TABLE substance.id_bnvd_sub ADD CONSTRAINT id_bnvd_sub_fk0 FOREIGN KEY (substance_bnvd_cas_bnvd) REFERENCES substance.id_bnvd_sub (substance_bnvd_cas_bnvd);

ALTER TABLE substance.infos_bnvd_sub ADD CONSTRAINT infos_bnvd_sub_fk0 FOREIGN KEY (substance_bnvd_cas_bnvd) REFERENCES substance.id_bnvd_sub (substance_bnvd_cas_bnvd);

ALTER TABLE substance.id_ephy_x_id_bnvd_sub ADD CONSTRAINT id_ephy_x_id_bnvd_sub_fk0 FOREIGN KEY (substance_bnvd_cas_bnvd) REFERENCES substance.id_bnvd_sub (substance_bnvd_cas_bnvd);

ALTER TABLE substance.id_agritox_x_id_bnvd_sub ADD CONSTRAINT id_agritox_x_id_bnvd_sub_fk0 FOREIGN KEY (substance_bnvd_cas_bnvd) REFERENCES substance.id_bnvd_sub (substance_bnvd_cas_bnvd);

ALTER TABLE substance.id_atp_x_id_bnvd_sub ADD CONSTRAINT id_atp_x_id_bnvd_sub_fk0 FOREIGN KEY (substance_bnvd_cas_bnvd) REFERENCES substance.id_bnvd_sub (substance_bnvd_cas_bnvd);

ALTER TABLE substance.id_eupdb_x_id_bnvd_sub ADD CONSTRAINT id_eupdb_x_id_bnvd_sub_fk0 FOREIGN KEY (substance_bnvd_cas_bnvd) REFERENCES substance.id_bnvd_sub (substance_bnvd_cas_bnvd);

ALTER TABLE substance.id_sandre_x_id_bnvd_sub ADD CONSTRAINT id_sandre_x_id_bnvd_sub_fk0 FOREIGN KEY (substance_bnvd_cas_bnvd) REFERENCES substance.id_bnvd_sub (substance_bnvd_cas_bnvd);
