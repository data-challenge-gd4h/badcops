-- Paramètres pour les répertoires des fichiers CSV
\set chemin_id_bnvd_sub :chemin_id_bnvd_sub
\set chemin_infos_bnvd_sub :chemin_infos_bnvd_sub
\set chemin_id_ephy_x_id_bnvd_sub :chemin_id_ephy_x_id_bnvd_sub
\set chemin_infos_ephy_sub :chemin_infos_ephy_sub
\set chemin_id_agritox_x_id_bnvd_sub :chemin_id_agritox_x_id_bnvd_sub
\set chemin_infos_agritox_sub :chemin_infos_agritox_sub
\set chemin_id_atp_x_id_bnvd_sub :chemin_id_atp_x_id_bnvd_sub
\set chemin_infos_atp_sub :chemin_infos_atp_sub
\set chemin_id_eupdb_x_id_bnvd_sub :chemin_id_eupdb_x_id_bnvd_sub
\set chemin_infos_eupdb_sub :chemin_infos_eupdb_sub
\set chemin_id_sandre_x_id_bnvd_sub :chemin_id_sandre_x_id_bnvd_sub
\set chemin_infos_sandre_sub :chemin_infos_sandre_sub
\set chemin_id_bnvd_amm :chemin_id_bnvd_amm
\set chemin_infos_bnvd_amm :chemin_infos_bnvd_amm
\set chemin_id_ephy_x_id_bnvd_amm :chemin_id_ephy_x_id_bnvd_amm
\set chemin_infos_ephy_amm_identite :chemin_infos_ephy_amm_identite
\set chemin_infos_ephy_amm_composition :chemin_infos_ephy_amm_composition
\set chemin_infos_ephy_amm_usages :chemin_infos_ephy_amm_usages
\set chemin_infos_ephy_amm_mention_danger :chemin_infos_ephy_amm_mention_danger
\set chemin_infos_ephy_amm_phrases_risque :chemin_infos_ephy_amm_phrases_risque

-- Intégration des données dans les tables
COPY substance.id_bnvd_sub FROM :'chemin_id_bnvd_sub' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY substance.infos_bnvd_sub FROM :'chemin_infos_bnvd_sub' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY substance.id_ephy_x_id_bnvd_sub FROM :'chemin_id_ephy_x_id_bnvd_sub' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY substance.infos_ephy_sub FROM :'chemin_infos_ephy_sub' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY substance.id_agritox_x_id_bnvd_sub FROM :'chemin_id_agritox_x_id_bnvd_sub' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY substance.infos_agritox_sub FROM :'chemin_infos_agritox_sub' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY substance.id_atp_x_id_bnvd_sub FROM :'chemin_id_atp_x_id_bnvd_sub' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY substance.infos_atp_sub FROM :'chemin_infos_atp_sub' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY substance.id_eupdb_x_id_bnvd_sub FROM :'chemin_id_eupdb_x_id_bnvd_sub' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY substance.infos_eupdb_sub FROM :'chemin_infos_eupdb_sub' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY substance.id_sandre_x_id_bnvd_sub FROM :'chemin_id_sandre_x_id_bnvd_sub' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY substance.infos_sandre_sub FROM :'chemin_infos_sandre_sub' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY amm.id_bnvd_amm FROM :'chemin_id_bnvd_amm' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY amm.infos_bnvd_amm FROM :'chemin_infos_bnvd_amm' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY amm.id_ephy_x_id_bnvd_amm FROM :'chemin_id_ephy_x_id_bnvd_amm' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY amm.infos_ephy_amm_identite FROM :'chemin_infos_ephy_amm_identite' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY amm.infos_ephy_amm_composition FROM :'chemin_infos_ephy_amm_composition' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY amm.infos_ephy_amm_usages FROM :'chemin_infos_ephy_amm_usages' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY amm.infos_ephy_amm_mention_danger FROM :'chemin_infos_ephy_amm_mention_danger' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY amm.infos_ephy_amm_phrases_risque FROM :'chemin_infos_ephy_amm_phrases_risque' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
