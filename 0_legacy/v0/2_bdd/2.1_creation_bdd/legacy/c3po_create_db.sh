#!/bin/bash

echo "Bienvenue dans le script d'intégration des données C3PO."

read -p "Entrez le répertoire bin de postgresql (par exemple /c/Program Files/PostgreSQL/14/bin): " repertoire_psql
read -p "Entrez l'hôte (par exemple localhost) : " hote
read -p "Entrez le nom d'utilisateur (par exemple postgres) : " utilisateur
read -p "Entrez le port (par exemple 5432) : " port
read -s -p "Entrez le mot de passe : " mot_de_passe

# Ajouter le répertoire bin de PostgreSQL à la variable PATH
export PATH="$repertoire_psql:$PATH"
echo

# Récupérer le chemin du répertoire actuel du script
repertoire_script=$(dirname "$0")

# Modifier les chemins des fichiers CSV pour qu'ils soient interprétés par Bash
chemin_id_bnvd_sub="$repertoire_script/../../1_notebook/1.1_bnvd/output/id_bnvd_sub.csv"
chemin_infos_bnvd_sub="$repertoire_script/../../1_notebook/1.1_bnvd/output/infos_bnvd_sub.csv"
chemin_id_ephy_x_id_bnvd_sub="$repertoire_script/../../1_notebook/1.2_ephy/output/id_ephy_x_id_bnvd_sub.csv"
chemin_infos_ephy_sub="$repertoire_script/../../1_notebook/1.2_ephy/output/infos_ephy_sub.csv"
chemin_id_agritox_x_id_bnvd_sub="$repertoire_script/../../1_notebook/1.3_agritox/output/id_agritox_x_id_bnvd_sub.csv"
chemin_infos_agritox_sub="$repertoire_script/../../1_notebook/1.3_agritox/output/infos_agritox_sub.csv"
chemin_id_atp_x_id_bnvd_sub="$repertoire_script/../../1_notebook/1.4_atp/output/id_atp_x_id_bnvd_sub.csv"
chemin_infos_atp_sub="$repertoire_script/../../1_notebook/1.4_atp/output/infos_atp_sub.csv"
chemin_id_eupdb_x_id_bnvd_sub="$repertoire_script/../../1_notebook/1.5_eupdb/output/id_eupdb_x_id_bnvd_sub.csv"
chemin_infos_eupdb_sub="$repertoire_script/../../1_notebook/1.5_eupdb/output/infos_eupdb_sub.csv"
chemin_id_sandre_x_id_bnvd_sub="$repertoire_script/../../1_notebook/1.6_sandre/output/id_sandre_x_id_bnvd_sub.csv"
chemin_infos_sandre_sub="$repertoire_script/../../1_notebook/1.6_sandre/output/infos_sandre_sub.csv"
chemin_id_bnvd_amm="$repertoire_script/../../1_notebook/1.1_bnvd/output/id_bnvd_amm.csv"
chemin_infos_bnvd_amm="$repertoire_script/../../1_notebook/1.1_bnvd/output/infos_bnvd_amm.csv"
chemin_id_ephy_x_id_bnvd_amm="$repertoire_script/../../1_notebook/1.2_ephy/output/id_ephy_x_id_bnvd_amm.csv"
chemin_infos_ephy_amm_identite="$repertoire_script/../../1_notebook/1.2_ephy/output/infos_ephy_amm_identite.csv"
chemin_infos_ephy_amm_composition="$repertoire_script/../../1_notebook/1.2_ephy/output/infos_ephy_amm_composition.csv"
chemin_infos_ephy_amm_usages="$repertoire_script/../../1_notebook/1.2_ephy/output/infos_ephy_amm_usages.csv"
chemin_infos_ephy_amm_mention_danger="$repertoire_script/../../1_notebook/1.2_ephy/output/infos_ephy_amm_mention_danger.csv"
chemin_infos_ephy_amm_phrases_risque="$repertoire_script/../../1_notebook/1.2_ephy/output/infos_ephy_amm_phrases_risque.csv"

# Exécuter le script SQL en utilisant le chemin complet
PGPASSWORD="$mot_de_passe" psql -h "$hote" -d postgres -U "$utilisateur" -p "$port" \
  -a -f "$repertoire_script/sql/c3po_pre_process.sql"
  
echo "Initialisation de la base de données C3PO terminée."

# Exécuter les commandes psql en utilisant les chemins modifiés
PGPASSWORD="$mot_de_passe" psql -h "$hote" -d c3po -U "$utilisateur" -p "$port" \
  -v chemin_id_bnvd_sub="$chemin_id_bnvd_sub" \
  -v chemin_infos_bnvd_sub="$chemin_infos_bnvd_sub" \
  -v chemin_id_ephy_x_id_bnvd_sub="$chemin_id_ephy_x_id_bnvd_sub" \
  -v chemin_infos_ephy_sub="$chemin_infos_ephy_sub" \
  -v chemin_id_agritox_x_id_bnvd_sub="$chemin_id_agritox_x_id_bnvd_sub" \
  -v chemin_infos_agritox_sub="$chemin_infos_agritox_sub" \
  -v chemin_id_atp_x_id_bnvd_sub="$chemin_id_atp_x_id_bnvd_sub" \
  -v chemin_infos_atp_sub="$chemin_infos_atp_sub" \
  -v chemin_id_eupdb_x_id_bnvd_sub="$chemin_id_eupdb_x_id_bnvd_sub" \
  -v chemin_infos_eupdb_sub="$chemin_infos_eupdb_sub" \
  -v chemin_id_sandre_x_id_bnvd_sub="$chemin_id_sandre_x_id_bnvd_sub" \
  -v chemin_infos_sandre_sub="$chemin_infos_sandre_sub" \
  -v chemin_id_bnvd_amm="$chemin_id_bnvd_amm" \
  -v chemin_infos_bnvd_amm="$chemin_infos_bnvd_amm" \
  -v chemin_id_ephy_x_id_bnvd_amm="$chemin_id_ephy_x_id_bnvd_amm" \
  -v chemin_infos_ephy_amm_identite="$chemin_infos_ephy_amm_identite" \
  -v chemin_infos_ephy_amm_composition="$chemin_infos_ephy_amm_composition" \
  -v chemin_infos_ephy_amm_usages="$chemin_infos_ephy_amm_usages" \
  -v chemin_infos_ephy_amm_mention_danger="$chemin_infos_ephy_amm_mention_danger" \
  -v chemin_infos_ephy_amm_phrases_risque="$chemin_infos_ephy_amm_phrases_risque" \
  -a -f "$repertoire_script/sql/c3po_process.sql"

echo "Intégration des données dans C3PO terminée."

# Ajouter une pause pour empêcher la fermeture automatique
read -p "Appuyez sur Entrée pour quitter..."
