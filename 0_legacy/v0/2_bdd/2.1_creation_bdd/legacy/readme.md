# Installation de la base de données c3po à partir des fichiers générés par les scripts Notebook Jupyter en passant par bash

Ce script est une ancienne version de la création de la base de données C3PO. Il est conservé à titre d’archive, la version Python est privilégiée.

1. Dans votre répertoire **c3po**, lancez les commandes suivantes :

```zsh
cd 2_bdd/2.1_creation_bdd
bash c3po_create_db.sh
```

2. Renseigner les variables demandées par le script (répertoire bin de PostgreSQL, hôte, nom d’utilisateur, port).

```
Entrez le répertoire bin de postgresql (par exemple /c/Program Files/PostgreSQL/14/bin): c/Program Files/PostgreSQL/14/bin
Entrez l'hôte (par exemple localhost) : localhost
Entrez le nom d'utilisateur (par exemple postgres) : postgres
Entrez le port (par exemple 5432) : 5432
```

> **ATTENTION** <br>
> L’exemple ci-dessus fonctionne pour une configuration locale donnée, il se peut que ces paramètres changent en fonction de la configuration spécifique à chaque serveur. Assurez-vous donc de bien vérifier au préalable tous ces paramètres, le répertoire bin de PostgreSQL est notamment susceptible de changer.
3. Renseigner le mot de passe associé à l’utilisateur (pour des raisons de confidentialité, celui-ci ne s’affiche pas sur l’interface de la console lorsque vous le saisissez).
```
Entrez le mot de passe : 
```
4. Le script s’exécute alors en deux étapes.<br>
    **a.** Une fois la première partie complétée, le message « Initialisation de la base de données C3PO terminée. » s’affiche. Cela signifie que la base de données c3po a bien été créée (si une base c3po existait auparavant, celle-ci a été effacée). A ce stade la base c3po contient les schémas et tables mais ne comporte pas encore de données. La console vous invite ensuite à saisir à nouveau votre mot de passe utilisateur afin de vous connecter à la base c3po.
    ```
    ...
    Initialisation de la base de données C3PO terminée.
    Mot de passe pour l'utilisateur postgres :
    ```
    **b.** La seconde partie du script s’exécute alors. Le message « Intégration des données dans C3PO terminée » indique que le script s’est déroulé avec succès. Appuyer sur Entrée pour quitter la console.
    ```
    ...
    Intégration des données dans C3PO terminée.
    Appuyez sur Entrée pour quitter...
    ```
