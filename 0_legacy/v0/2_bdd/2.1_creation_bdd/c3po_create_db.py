import pandas as pd
from sqlalchemy import create_engine
import psycopg2
from psycopg2 import sql
import getpass

# Demander le nom d'utilisateur à l'utilisateur
user = input("Veuillez entrer votre nom d'utilisateur PostgreSQL (par défaut: postgres): ")
if not user:
    user = 'postgres'

# Demander l'hôte à l'utilisateur
host = input("Veuillez entrer votre hôte PostgreSQL (par défaut: localhost): ")
if not host:
    host = 'localhost'

# Demander le port à l'utilisateur
try:
    port = int(input("Veuillez entrer votre port PostgreSQL (par défaut: 5432): "))
except ValueError:
    port = 5432

# Demander le mot de passe à l'utilisateur
password = getpass.getpass("Veuillez entrer votre mot de passe PostgreSQL: ")

db_params = {
    'user': user,
    'password': password,
    'host': host,
    'port': port
}

# Nom de la base de données à créer
database_name = 'c3po'

conn = psycopg2.connect(**db_params)
cur = conn.cursor()
# Activer l'option autocommit
conn.autocommit = True

# Vérifier si la base de données existe
cur.execute("SELECT 1 FROM pg_catalog.pg_database WHERE datname = %s", (database_name,))
exists = cur.fetchone()

# Créer la base de données si elle n'existe pas
if not exists:
    cur.execute(f"CREATE DATABASE {database_name}")
    print(f"La base de données {database_name} a été créée.")
else:
    print(f"La base de données {database_name} existe déjà")

# Créer une connexion à la base de données
db_params['database'] = 'c3po'
engine = create_engine(f"postgresql+psycopg2://{db_params['user']}:{db_params['password']}@{db_params['host']}:{db_params['port']}/{db_params['database']}")

# Modifier les dfs des fichiers CSV pour qu'ils soient interprétés par Bash
df_id_bnvd_sub=pd.read_csv('../../1_notebook/1.1_bnvd/output/id_bnvd_sub.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_infos_bnvd_sub=pd.read_csv('../../1_notebook/1.1_bnvd/output/infos_bnvd_sub.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_id_ephy_x_id_bnvd_sub=pd.read_csv('../../1_notebook/1.2_ephy/output/id_ephy_x_id_bnvd_sub.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_infos_ephy_sub=pd.read_csv('../../1_notebook/1.2_ephy/output/infos_ephy_sub.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_id_agritox_x_id_bnvd_sub=pd.read_csv('../../1_notebook/1.3_agritox/output/id_agritox_x_id_bnvd_sub.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_infos_agritox_sub=pd.read_csv('../../1_notebook/1.3_agritox/output/infos_agritox_sub.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_id_atp_x_id_bnvd_sub=pd.read_csv('../../1_notebook/1.4_atp/output/id_atp_x_id_bnvd_sub.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_infos_atp_sub=pd.read_csv('../../1_notebook/1.4_atp/output/infos_atp_sub.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_id_eupdb_x_id_bnvd_sub=pd.read_csv('../../1_notebook/1.5_eupdb/output/id_eupdb_x_id_bnvd_sub.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_infos_eupdb_sub=pd.read_csv('../../1_notebook/1.5_eupdb/output/infos_eupdb_sub.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_id_sandre_x_id_bnvd_sub=pd.read_csv('../../1_notebook/1.6_sandre/output/id_sandre_x_id_bnvd_sub.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_infos_sandre_sub=pd.read_csv('../../1_notebook/1.6_sandre/output/infos_sandre_sub.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_id_bnvd_amm=pd.read_csv('../../1_notebook/1.1_bnvd/output/id_bnvd_amm.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_infos_bnvd_amm=pd.read_csv('../../1_notebook/1.1_bnvd/output/infos_bnvd_amm.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_id_ephy_x_id_bnvd_amm=pd.read_csv('../../1_notebook/1.2_ephy/output/id_ephy_x_id_bnvd_amm.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_infos_ephy_amm_identite=pd.read_csv('../../1_notebook/1.2_ephy/output/infos_ephy_amm_identite.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_infos_ephy_amm_composition=pd.read_csv('../../1_notebook/1.2_ephy/output/infos_ephy_amm_composition.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_infos_ephy_amm_usages=pd.read_csv('../../1_notebook/1.2_ephy/output/infos_ephy_amm_usages.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_infos_ephy_amm_mention_danger=pd.read_csv('../../1_notebook/1.2_ephy/output/infos_ephy_amm_mention_danger.csv', on_bad_lines='skip', sep=';', low_memory=False)
df_infos_ephy_amm_phrases_risque=pd.read_csv('../../1_notebook/1.2_ephy/output/infos_ephy_amm_phrases_risque.csv', on_bad_lines='skip', sep=';', low_memory=False)

# Insérer les données du DataFrame dans la table
df_id_bnvd_sub.to_sql('id_bnvd_sub', engine, if_exists='replace', index=False)
df_infos_bnvd_sub.to_sql('infos_bnvd_sub', engine, if_exists='replace', index=False)
df_id_ephy_x_id_bnvd_sub.to_sql('id_ephy_x_id_bnvd_sub', engine, if_exists='replace', index=False)
df_infos_ephy_sub.to_sql('infos_ephy_sub', engine, if_exists='replace', index=False)
df_id_agritox_x_id_bnvd_sub.to_sql('id_agritox_x_id_bnvd_sub', engine, if_exists='replace', index=False)
df_infos_agritox_sub.to_sql('infos_agritox_sub', engine, if_exists='replace', index=False)
df_id_atp_x_id_bnvd_sub.to_sql('id_atp_x_id_bnvd_sub', engine, if_exists='replace', index=False)
df_infos_atp_sub.to_sql('infos_atp_sub', engine, if_exists='replace', index=False)
df_id_eupdb_x_id_bnvd_sub.to_sql('id_eupdb_x_id_bnvd_sub', engine, if_exists='replace', index=False)
df_infos_eupdb_sub.to_sql('infos_eupdb_sub', engine, if_exists='replace', index=False)
df_id_sandre_x_id_bnvd_sub.to_sql('id_sandre_x_id_bnvd_sub', engine, if_exists='replace', index=False)
df_infos_sandre_sub.to_sql('infos_sandre_sub', engine, if_exists='replace', index=False)
df_id_bnvd_amm.to_sql('id_bnvd_amm', engine, if_exists='replace', index=False)
df_infos_bnvd_amm.to_sql('infos_bnvd_amm', engine, if_exists='replace', index=False)
df_id_ephy_x_id_bnvd_amm.to_sql('id_ephy_x_id_bnvd_amm', engine, if_exists='replace', index=False)
df_infos_ephy_amm_identite.to_sql('infos_ephy_amm_identite', engine, if_exists='replace', index=False)
df_infos_ephy_amm_composition.to_sql('infos_ephy_amm_composition', engine, if_exists='replace', index=False)
df_infos_ephy_amm_usages.to_sql('infos_ephy_amm_usages', engine, if_exists='replace', index=False)
df_infos_ephy_amm_mention_danger.to_sql('infos_ephy_amm_mention_danger', engine, if_exists='replace', index=False)
df_infos_ephy_amm_phrases_risque.to_sql('infos_ephy_amm_phrases_risque', engine, if_exists='replace', index=False)

# Fermer le curseur et la connexion
cur.close()
conn.close()

input("La création de la base de données c3po a été effectuée avec succès.")