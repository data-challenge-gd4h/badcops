# Challenge GD4H - C3PO

<a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) is an initiative led by the Innovation Lab (Ecolab) of the French ministry of ecology.

A challenge has been organised in 2023 to develop tools, rooted in the health-environment data community, aiming at addressing shared issues.

Link : 
<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Website</a> 

## C3PO

Many of the substances used in **plant protection products (PPPs)** have a presumed or proven impact on health and the environment. Moreover, to carry out studies on these impacts, it is necessary to rely, among other things, on reference data.

Although there are several sources of data on plant protection products and pesticides in France and Europe, these are often managed by different bodies and are not systematically based on common frames of reference. In practice, this makes it difficult to link the information contained in these different databases.

Among the sources of data on PPPs, the **<a href="https://ventes-produits-phytopharmaceutiques.eaufrance.fr/" target="_blank" rel="noreferrer">BNV-D</a> (_Banque Nationale des Ventes de produits phytopharmaceutiques par les Distributeurs agréés_ (National database on sales of plant protection products by authorized distributors))**, whose data is disseminated by the OFB, occupies a specific place. It contains annual sales quantities of these products, broken down by purchaser's zip code. It is therefore the best open database available in France for approximating the pressure exerted by these products on health and the environment. However, this database does not include information on certain characteristics of PPPs, such as toxicity/ecotoxicity data, uses, or the functions of products and the active substances they contain.

These data are contained in other open reference data sources at the French level (<a href="https://www.data.gouv.fr/fr/datasets/base-de-donnees-agritox/" target="_blank" rel="noreferrer">Agritox</a>, <a href="https://www.data.gouv.fr/fr/datasets/donnees-ouvertes-du-catalogue-e-phy-des-produits-phytopharmaceutiques-matieres-fertilisantes-et-supports-de-culture-adjuvants-produits-mixtes-et-melanges/" target="_blank" rel="noreferrer">E-phy</a>, <a href="https://www.sandre.eaufrance.fr/api-referentiel" target="_blank" rel="noreferrer">Sandre reference system</a>, <a href="https://geoservices.ign.fr/documentation/donnees/vecteur/rpg"  target="_blank" rel="noreferrer">crop reference system of the Graphic Parcel Register</a>) and european (<a href="https://ec.europa.eu/food/plant/pesticides/eu-pesticides-database/start/screen/active-substances" target="_blank" rel="noreferrer">EU Pesticides Database</a>, <a href="https://echa.europa.eu/fr/information-on-chemicals/annex-vi-to-clp" target="_blank" rel="noreferrer">table of harmonized entries available in Annex VI of the CLP Regulation</a>).

The **BaDCoPS database** was created from the automated enrichment of BNV-D data with these other data sources.

<a href="https://challenge.gd4h.ecologie.gouv.fr/defi/?topic=13" target="_blank" rel="noreferrer">More about the challenge</a>

## **Documentation**

### **Solution description**

The following steps were necessary to create the C3PO database:
|Steps|Deliverables|
|-----|--------|
|**1. Functional and technical specifications** (access to source data, description of its structure and the processing to be carried out on it) | [Data dictionary](/1_notebook/C3PO_dictionnaire_donnees_2023-08.xlsx)|
|**2. Source data collection**| Files in usable format (.csv, .json or .xlsx) for step 3<br>• **BNV-D** : <a href="https://www.data.gouv.fr/fr/datasets/ventes-de-pesticides-par-departement/" target="_blank" rel="noreferrer">Sales</a> and <a href="https://www.data.gouv.fr/fr/datasets/achats-de-pesticides-par-code-postal/" target="_blank" rel="noreferrer">Purchases</a><br>• <a href="https://www.data.gouv.fr/fr/datasets/donnees-ouvertes-du-catalogue-e-phy-des-produits-phytopharmaceutiques-matieres-fertilisantes-et-supports-de-culture-adjuvants-produits-mixtes-et-melanges/" target="_blank" rel="noreferrer">**E-phy**</a><br>• <a href="https://www.data.gouv.fr/fr/datasets/base-de-donnees-agritox/" target="_blank" rel="noreferrer">**Agritox**</a><br>• **Sandre** : reference systems <a href="https://api.sandre.eaufrance.fr/referentiels/v1/par.json?outputSchema=SANDREv4&filter=$StParametre=%27Valid%C3%A9%27%20and%20$ParametreChimique.CdCASSubstanceChimique!=%27%27" target="_blank" rel="noreferrer">Parameters</a> et <a href="https://api.sandre.eaufrance.fr/referentiels/v1/gpr.json?outputSchema=SANDREv4&filter=$StGroupeParametres=%27Valid%C3%A9%27%20" target="_blank" rel="noreferrer">Parameters group</a><br>• **RPG** : <a href="https://geoservices.ign.fr/sites/default/files/2023-02/REF_CULTURES_GROUPES_CULTURES_2021.csv"  target="_blank" rel="noreferrer">crop reference system</a><br>• <a href="https://ec.europa.eu/food/plant/pesticides/eu-pesticides-database/start/screen/active-substances" target="_blank" rel="noreferrer">**EU Pesticides Database**</a><br>• <a href="https://echa.europa.eu/fr/information-on-chemicals/annex-vi-to-clp" target="_blank" rel="noreferrer"> **ATP** : table of harmonized entries available in Annex VI of the CLP Regulation</a>|
|**3. Source data processing scripts** according to data dictionary recommendations.<br> _Langage : python (notebook jupyter)_|Jupyter Notebook scripts generating flat .csv files corresponding to the database tables BaDCoPS<br>• **[BNV-D](/1_notebook/1.1_bnvd)**<br>• **[E-phy](/1_notebook/1.2_ephy)**<br>• **[Agritox](/1_notebook/1.3_agritox)**<br>• **[Sandre](/1_notebook/1.6_sandre)**<br>• **[EU Pesticides Database](/1_notebook/1.5_eupdb)**<br>• **[ATP](/1_notebook/1.4_atp)**<br>NB: processing of the RPG crop reference system is included in the [script for integrating information from E-phy].(/1_notebook/1.2_ephy/2_ephy_amm.ipynb)_|
|**4. BaDCoPS database creation**<br> (including integration of data from stage 3)<br>_Langage : SQL_|• [Database creation scripts](/2_bdd/2.1_creation_bdd)<br>• [Creation scripts](/2_bdd/2.2_dump_bdd/c3po_create_dump_db.sh) and [restoration scripts](/2_bdd/2.2_dump_bdd/c3po_restore_dump_db.sh) of database dumps<br>• [SQL database dump](/2_bdd/2.2_dump_bdd/c3po_dump.sql)|
|_Étape non terminée_<br>**5. Data distribution via API**|• API (interface contract and documentation)|

BaDCoPS includes two diagrams: **substance** and **amm**, containing respectively all the information relating to the substances contained in plant protection products and to the products (identified by their Marketing Authorisation number - MA) present in the BNV-D.

**Format and encoding**<br>
Each table is assigned a flat file (.csv format) which can be accessed in the "/output" directory of the data processing scripts (see deliverables for stage 3) and on the <a href="https://www.data.gouv.fr/fr/datasets/base-de-connaissances-sur-les-produits-phytopharmaceutiques-a-partir-de-sources-ouvertes-c3po/" target="_blank" rel="noreferrer">data.gouv.fr</a>. Each file is encoded in UTF-8 and has a header line, the column separator is the semicolon.

### **Installation**

[Installation Guide](/INSTALL.md)

### **Usage**

#### Scientific value of the data

This new BaDCoPS database will make it easier to use and exploit BNV-D data than is possible today. For example, BaDCoPS will provide an immediate answer to the question "on which crop is such and such a substance from the BNV-D authorised?
Coupling BaDCoPS with BNV-D data spatialised according to the purchaser's postcode offers a wide range of potential uses. For example, it will now be possible to map purchases of active substances according to their toxicity, or to study the effect of European bans on the purchase of certain substances.

#### Other sources of interest 

As well as being used for scientific studies, the BaDCoPS database will make it easier to support public policies.
For example, it will make it possible to automate the preparation of the annual order establishing the list of substances defined in article L. 213-10-8 of the French Environment Code relating to the levy for diffuse pollution, a task requiring the cross-referencing of data from the BNV-D, Agritox, the EU PDB and the ATP.
Another example of support for public policies is the cross-referencing of sales and toxicity data for pesticide active substances, carried out as part of the study prior to the revision of the monitoring and assessment decrees. BaDCoPS would make it easier to calculate the Toxic Potential (PoTox), i.e. the quantity sold divided by the PNEC value.

### **Contributing**

If you wish to conribute to this project, please follow the [recommendations](/CONTRIBUTING.md).

### **Licence**

The code is published under [MIT Licence](/LICENSE).

The data referenced in this README and in the installation guide is published under <a href="https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf">Open Licence 2.0</a>.
