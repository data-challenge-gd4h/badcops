# Guide d'installation

## I. Collecte des données

Les données sources sont acccessibles aux liens suivants :
- **BNV-D** : <a href="https://www.data.gouv.fr/fr/datasets/ventes-de-pesticides-par-departement/" target="_blank" rel="noreferrer">Ventes</a> et <a href="https://www.data.gouv.fr/fr/datasets/achats-de-pesticides-par-code-postal/" target="_blank" rel="noreferrer">Achats</a>
- <a href="https://www.data.gouv.fr/fr/datasets/donnees-ouvertes-du-catalogue-e-phy-des-produits-phytopharmaceutiques-matieres-fertilisantes-et-supports-de-culture-adjuvants-produits-mixtes-et-melanges/" target="_blank" rel="noreferrer">**E-phy**</a>
- <a href="https://www.data.gouv.fr/fr/datasets/base-de-donnees-agritox/" target="_blank" rel="noreferrer">**Agritox**</a>
- **Sandre** : référentiels <a href="https://api.sandre.eaufrance.fr/referentiels/v1/par.json?outputSchema=SANDREv4&filter=$StParametre=%27Valid%C3%A9%27%20and%20$ParametreChimique.CdCASSubstanceChimique!=%27%27" target="_blank" rel="noreferrer">Paramètres</a> et <a href="https://api.sandre.eaufrance.fr/referentiels/v1/gpr.json?outputSchema=SANDREv4&filter=$StGroupeParametres=%27Valid%C3%A9%27%20" target="_blank" rel="noreferrer">Groupe de Paramètres</a>
- **RPG** : <a href="https://geoservices.ign.fr/sites/default/files/2023-02/REF_CULTURES_GROUPES_CULTURES_2021.csv"  target="_blank" rel="noreferrer">référentiel des cultures</a>
- <a href="https://ec.europa.eu/food/plant/pesticides/eu-pesticides-database/start/screen/active-substances" target="_blank" rel="noreferrer">**EU Pesticides Database**</a>
- **ATP** : <a href="https://echa.europa.eu/fr/information-on-chemicals/annex-vi-to-clp" target="_blank" rel="noreferrer"> tableau d’entrées harmonisées disponible à l’annexe VI du règlement CLP</a>

> **NB** <br>
> Le téléchargement de ces données sources est exécuté par les scripts Notebook Jupyter, à l'exception de EU Pesticides Database et de ATP pour lesquelles les  instructions de téléchargement sont indiquées dans les scripts et rappelées [dans ce document](#cas-particuliers-de-fichiers-%C3%A0-int%C3%A9grer-manuellement-dans-le-sous-r%C3%A9pertoire-input).



## II. Dépendances

Les programmes suivants sont nécessaires à l'éxecution du code :
- [LibreOffice Calc](https://fr.libreoffice.org/download/telecharger-libreoffice/)
- [Python](https://www.python.org/downloads/) (nécessite également [Jupyter](https://jupyter.org/install))
- [PostgreSQL](https://www.postgresql.org/)
- [GitBash](https://git-scm.com/downloads)


Pour installer les dépendances python nécessaires au fonctionnement du code, il est conseillé d'activer un environnement virtuel : 

```zsh
python -m venv env
source env/bin/activate
```

Lancez ensuite la commande suivante :

```zsh
pip install -r requirements.txt
```

## III. Développement

> **ATTENTION**<br>
>Afin de garantir la bonne exécution des scripts, on veillera à conserver l'arborescence du projet GitLab (dont une partie est décrite dans cette section).

### 1. Lancement des scripts Notebook Jupyter

Le répertoire [1_notebook](/1_notebook) comporte autant de sous-répertoires qu'il y a de sources de données à intégrer dans C3PO.
Exécuter les scripts contenus dans ces sous-répertoires dans l'ordre indiqué par les numéros **1.X** en préfixe de leur nom "**1.X_{_source de données_}**". Ainsi on commencera par exécuter les scripts de [1.1_bnvd](/1_notebook/1.1_bnvd), puis ceux de [1.2_ephy](/1_notebook/1.2_ephy), etc.

Ces sous-répertoires sont organisés suivant une arboresence du type :
- **{_source de données_}_substance.ipynb** -> Script Notebook Jupyter permettant de générer les fichiers **id_bnvd_substance.csv**, **id_bnvd_x_id_{_source de données_}\_substance.csv** et **infos_{_source de données_}_substance.csv** qui seront stockés dans le sous-répertoire **/output**.<br>
_Sources de données concernées : toutes (BNV-D, E-phy, Agritox, Sandre, EU Pesticides Database et ATP)_
- **{_source de données_}\_amm.ipynb** _(optionnel)_ -> Script Notebook Jupyter permettant de générer les fichiers **id_bnvd_amm.csv**, **id_bnvd_x_id_{_source de données_}\_amm.csv** et **infos_{_source de données_}_amm.csv** qui seront stockés dans le sous-répertoire **/output**.<br>
_Sources de données concernées : BNV-D et E-phy_
- **/input** -> sous-répertoire où sont stockées les données sources nécessaires à la poursuite d'exécution du script. (voir [cas particuliers](#cas-particuliers-de-fichiers-%C3%A0-int%C3%A9grer-manuellement-dans-le-sous-r%C3%A9pertoire-input))
- **/interm** _(optionnel)_ -> sous-répertoire où sont stockées des données intermédiaires exportées en cours de script.<br>
_Sources de données concernées : BNV-D, E-phy, Agritox et ATP_
- **/output** -> sous-répertoire où sont stockées les données en sortie des scripts Notebook Jupyter (= tables qui seront ensuite intégrées dans la base de données C3PO).

#### Cas particuliers de fichiers à intégrer manuellement dans le sous-répertoire **/input**
Les scripts Notebook Jupyter téléchargent automatiquement les données en entrée dans le répertoire **/input** à l'exception des scripts relatifs aux bases de données européennes (**[EU Pesticides Database](/1_notebook/1.5_eupdb/eupdb_substance.ipynb)** et **[ATP](/1_notebook/1.4_atp/atp_substance.ipynb)**).

- **EU PDB** : le dernière version du fichier est accessible à l'adresse : [https://ec.europa.eu/food/plant/pesticides/eu-pesticides-database/start/screen/active-substances](https://ec.europa.eu/food/plant/pesticides/eu-pesticides-database/start/screen/active-substances) (depuis cette page, cliquer sur `Export Active substances`).
- **ATP** : vérifier sur https://echa.europa.eu/fr/information-on-chemicals/annex-vi-to-clp que la variable `url_atp` du script [atp_substance.ipynb](/1_notebook/1.4_atp/atp_substance.ipynb) prend bien en compte la dernière version de l'ATP disponible.

Une fois que les dernières versions de EU PDB et de ATP ont été téléchargées, les déposer dans les répertoires **/input** de [EU PDB](/1_notebook/1.5_eupdb/input) et de [ATP](/1_notebook/1.4_atp/input).

### 2. Génération de la base de données C3PO

#### Prérequis
Avant de suivre les instructions suivantes, assurez-vous d’avoir exécuté avec succès les scripts Notebook Jupyter du répertoire **[1_notebooks](/1_notebook)** conformément aux [instructions précédentes](#1-lancement-des-scripts-notebook-jupyter) et a minima de disposer des fichiers .csv en sortie de ces scripts dans les sous-répertoires **/1_notebook/1.X_{_source de données_}/output** (X : numéro propre à chaque source de données).

Il existe deux possibilités pour générer la base de données c3po à partir des outputs :

#### Installation de la base de données c3po à partir des fichiers générés par les scripts Notebook Jupyter en passant par python

*Une ancienne version en bash est archivée dans le dossier legacy, cependant le script en Python est privilégié.*

1. Lancez le serveur PostgreSQL dans le CLI

Sur Linux (Ubuntu, CentOS 7+, etc.) :
```zsh
sudo systemctl start postgresql
```
Sur d'anciennes distributions Linux ou sur wsl :
```zsh
sudo service start postgresql
```
Sur MacOS :
```zsh
brew services start postgresql
```
Sur Windows :
```zsh
net start postgresql
```

Pensez à relever vos informations de connexion à PostGreSQL (utilisateur, mot de passe) ainsi votre hôte et votre port de connexion.

2. Lancez le script python 

```zsh
cd 2_bdd/2.1_creation_bdd/
python3 c3po_create_db.py
```

3. Vérifiez que vos tables ont bien été créées dans la base de données c3po

```zsh
sudo -u <nom_utilisateur> psql -d c3po
\dt
```

#### Installation de la base de données c3po à partir d’un dump

##### Création d’un dump SQL à partir de la base existante
1. Dans une console GitBash, lancer le script **[c3po_create_dump_db.sh](/2_bdd/2.2_dump_bdd/c3po_create_dump_db.sh)** (répertoire [2_bdd/2.2_dump_bdd](/2_bdd/2.2_dump_bdd)).
2. Renseigner les informations demandées par la console (voir [partie précédente](#installation-de-la-base-de-donn%C3%A9es-c3po-%C3%A0-partir-des-fichiers-g%C3%A9n%C3%A9r%C3%A9s-par-les-scripts-notebook-jupyter)).

##### Restauration d’un dump SQL
> **ATTENTION**<br>
> Pour que ce processus fonctionne, il est impératif de disposer d’un fichier **c3po_dump.sql** dans le répertoire [2_bdd/2.2_dump_bdd](/2_bdd/2.2_dump_bdd)
1. Dans une console GitBash, lancer le script **[c3po_restore_dump_db.sh](/2_bdd/2.2_dump_bdd/c3po_restore_dump_db.sh)** (répertoire [2_bdd/2.2_dump_bdd](/2_bdd/2.2_dump_bdd)).
2. Renseigner les informations demandées par la console (voir [partie précédente](#installation-de-la-base-de-donn%C3%A9es-c3po-%C3%A0-partir-des-fichiers-g%C3%A9n%C3%A9r%C3%A9s-par-les-scripts-notebook-jupyter)).

## Production

La solution C3PO n'est pas encore prête pour un déploiement en production.
