# Challenge GD4H - C3PO

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Dans ce cadre, un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées a été organisé en 2023.

Lien : 
<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a>

## C3PO

De nombreuses substances entrant dans la composition des **produits phytopharmaceutiques (PPP)** ont un impact présumé ou démontré sur la santé et l’environnement . Par ailleurs, pour mener des études s’intéressant à ces impacts, il est nécessaire de se baser, entre autres, sur des données de référence.

Or, s’il existe en France et en Europe plusieurs sources de données relatives aux produits phytopharmaceutiques et aux pesticides, ces dernières, souvent gérées par des structures différentes, ne reposent pas systématiquement sur des référentiels communs. Ainsi dans la pratique il s’avère complexe de faire le lien entre les informations contenues dans ces différentes bases de données.

Parmi les sources de données sur les PPP, la **<a href="https://ventes-produits-phytopharmaceutiques.eaufrance.fr/" target="_blank" rel="noreferrer">BNV-D</a> (Banque Nationale des Ventes de produits phytopharmaceutiques par les Distributeurs agréés)**, dont la diffusion des données est assurée par l’OFB, occupe une place particulière. En effet, elle comporte les quantités annuelles de vente de ces produits localisées au code postal de l'acheteur. Elle est de ce fait la meilleure base de données ouvertes disponible en France pour approximer la pression exercée par ces produits sur la santé et l’environnement. Néanmoins cette base ne comporte pas d'informations relatives à certaines caractéristiques des PPP, telles que les données de toxicité/écotoxicité, les usages, ou encore les fonctions des produits et des substances actives qui composent ces derniers.

Ces informations sont contenues dans d’autres sources de données ouvertes de référence aux niveaux français (<a href="https://www.data.gouv.fr/fr/datasets/base-de-donnees-agritox/" target="_blank" rel="noreferrer">Agritox</a>, <a href="https://www.data.gouv.fr/fr/datasets/donnees-ouvertes-du-catalogue-e-phy-des-produits-phytopharmaceutiques-matieres-fertilisantes-et-supports-de-culture-adjuvants-produits-mixtes-et-melanges/" target="_blank" rel="noreferrer">E-phy</a>, <a href="https://www.sandre.eaufrance.fr/api-referentiel" target="_blank" rel="noreferrer">référentiels du Sandre</a>, <a href="https://geoservices.ign.fr/documentation/donnees/vecteur/rpg"  target="_blank" rel="noreferrer">référentiel des cultures du Registre Parcellaire Graphique</a>) et européen (<a href="https://ec.europa.eu/food/plant/pesticides/eu-pesticides-database/start/screen/active-substances" target="_blank" rel="noreferrer">EU Pesticides Database</a>, <a href="https://echa.europa.eu/fr/information-on-chemicals/annex-vi-to-clp" target="_blank" rel="noreferrer">tableau d’entrées harmonisées disponible à l’annexe VI du règlement CLP</a>).

La **base de données C3PO** a été créée à partir de l’enrichissement automatisé des données de la BNV-D avec ces autres sources de données. 

<a href="https://challenge.gd4h.ecologie.gouv.fr/defi/?topic=13" target="_blank" rel="noreferrer">En savoir plus sur le défi</a> <br>
<a href="https://www.data.gouv.fr/fr/datasets/base-de-connaissances-sur-les-produits-phytopharmaceutiques-a-partir-de-sources-ouvertes-c3po/" target="_blank" rel="noreferrer">Accéder aux fichiers .csv des tables constituant la base de données sur data.gouv.fr</a>

## **Documentation**

### **Description de la solution**

Les étapes suivantes ont été nécessaires à la création de la base de données C3PO :
|Étapes|Livrables|
|-----|--------|
|**1. Spécifications fonctionnelles et techniques** (accès aux données sources, description de leur structure et des traitements à réaliser sur ces dernières) | [Dictionnaire de données](/1_notebook/C3PO_dictionnaire_donnees_2023-08.xlsx)|
|**2. Collecte des données sources**| Fichiers en format exploitable (.csv, .json ou .xlsx) pour l’étape 3<br>• **BNV-D** : <a href="https://www.data.gouv.fr/fr/datasets/ventes-de-pesticides-par-departement/" target="_blank" rel="noreferrer">Ventes</a> et <a href="https://www.data.gouv.fr/fr/datasets/achats-de-pesticides-par-code-postal/" target="_blank" rel="noreferrer">Achats</a><br>• <a href="https://www.data.gouv.fr/fr/datasets/donnees-ouvertes-du-catalogue-e-phy-des-produits-phytopharmaceutiques-matieres-fertilisantes-et-supports-de-culture-adjuvants-produits-mixtes-et-melanges/" target="_blank" rel="noreferrer">**E-phy**</a><br>• <a href="https://www.data.gouv.fr/fr/datasets/base-de-donnees-agritox/" target="_blank" rel="noreferrer">**Agritox**</a><br>• **Sandre** : référentiels <a href="https://api.sandre.eaufrance.fr/referentiels/v1/par.json?outputSchema=SANDREv4&filter=$StParametre=%27Valid%C3%A9%27%20and%20$ParametreChimique.CdCASSubstanceChimique!=%27%27" target="_blank" rel="noreferrer">Paramètres</a> et <a href="https://api.sandre.eaufrance.fr/referentiels/v1/gpr.json?outputSchema=SANDREv4&filter=$StGroupeParametres=%27Valid%C3%A9%27%20" target="_blank" rel="noreferrer">Groupe de Paramètres</a><br>• **RPG** : <a href="https://geoservices.ign.fr/sites/default/files/2023-02/REF_CULTURES_GROUPES_CULTURES_2021.csv"  target="_blank" rel="noreferrer">référentiel des cultures</a><br>• <a href="https://ec.europa.eu/food/plant/pesticides/eu-pesticides-database/start/screen/active-substances" target="_blank" rel="noreferrer">**EU Pesticides Database**</a><br>• <a href="https://echa.europa.eu/fr/information-on-chemicals/annex-vi-to-clp" target="_blank" rel="noreferrer"> **ATP** : tableau d’entrées harmonisées disponible à l’annexe VI du règlement CLP</a>|
|**3. Scripts de traitement  des données sources** suivant les préconisations du  dictionnaire des données.<br> _Langage : python (notebook jupyter)_|Scripts Notebook Jupyter générant des fichiers plats .csv correspondant aux tables de la base de données C3PO<br>• **[BNV-D](/1_notebook/1.1_bnvd)**<br>• **[E-phy](/1_notebook/1.2_ephy)**<br>• **[Agritox](/1_notebook/1.3_agritox)**<br>• **[Sandre](/1_notebook/1.6_sandre)**<br>• **[EU Pesticides Database](/1_notebook/1.5_eupdb)**<br>• **[ATP](/1_notebook/1.4_atp)**<br>_NB : le traitement du référentiel des cultures du RPG est inclus au [script d'intégration des informations issues de E-phy](/1_notebook/1.2_ephy/2_ephy_amm.ipynb)_<br><br><a href="https://www.data.gouv.fr/fr/datasets/base-de-connaissances-sur-les-produits-phytopharmaceutiques-a-partir-de-sources-ouvertes-c3po/" target="_blank" rel="noreferrer">**Accéder aux fichiers .csv en sortie de ces scripts des tables constituant la base de données sur data.gouv.fr**</a>|
|**4. Création de la base de données C3PO**<br> (dont intégration des données issues de l’étape 3)<br>_Langage : SQL_|• [Scripts de création de la base de données](/2_bdd/2.1_creation_bdd)<br>• Scripts de [création](/2_bdd/2.2_dump_bdd/c3po_create_dump_db.sh) et de [restauration](/2_bdd/2.2_dump_bdd/c3po_restore_dump_db.sh) de dumps de la base de données<br>• [Dump SQL de la base de données](/2_bdd/2.2_dump_bdd/c3po_dump.sql)|
|_Étape non terminée_<br>**5. Diffusion des données par API**|• API (contrat d'interface et documentation)|

C3PO comporte deux schémas : **substance** et **amm**, comportant respectivement toutes les informations relatives aux substances contenues dans les produits phytosanitaires et aux produits (identifiés par leur numéro d’Autorisation de Mise sur le Marché - AMM) présents dans la BNV-D.

**Format et encodage**<br>
A chaque table correspond un fichier plat (format .csv) accessible dans le répertoire "/output" des scripts de traitement de données (cf. livrables de l'étape 3) et sur <a href="https://www.data.gouv.fr/fr/datasets/base-de-connaissances-sur-les-produits-phytopharmaceutiques-a-partir-de-sources-ouvertes-c3po/" target="_blank" rel="noreferrer">data.gouv.fr</a>. Chaque fichier est encodé en UTF-8 et dispose d’une ligne d’en-tête, le séparateur de colonnes est le point-virgule.


### **Installation**

[Guide d'installation](/INSTALL.md)

### **Utilisation**

#### Intérêt scientifique des données

Cette nouvelle base de données C3PO permettra de faciliter les valorisations et l’exploitation des données de la BNV-D par rapport à ce qu’il est possible de faire aujourd’hui. Par exemple, C3PO permettra de répondre immédiatement à la question « sur quelle culture telle substance de la BNV-D est-elle autorisée ? ».
En couplant C3PO avec les données de la BNV-D spatialisée au code postal acheteur, les valorisations potentielles sont multiples. Par exemple, il sera désormais possible de cartographier les achats de substances actives en fonction de la toxicité de ces dernières, ou encore d’étudier l’effet des périodes d’interdiction au niveau européen de certaines substances sur les achats de ces dernières.

#### Autres sources d’intérêt 

Outre la réappropriation par des études scientifiques, la base de données C3PO permettra de faciliter l’appui aux politiques publiques.
Par exemple, elle permettra d’automatiser la préparation de l’arrêté annuel établissant la liste des substances définies à l'article L. 213-10-8 du code de l'environnement relatif à la redevance pour pollutions diffuses, travail nécessitant de croiser les données de la BNV-D, d’Agritox, de la EU PDB et de l’ATP.
Un autre exemple d’appui aux politiques publiques est le croisement entre données de ventes et de toxicité des substances actives pesticides opéré dans le cadre de l’étude amont à la révision des arrêtés surveillance et évaluations. C3PO faciliterait en effet le calcul du Potentiel toxique (PoTox), i.e. la quantité vendue divisée par la valeur de la PNEC.


### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommandations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [CeCILL-B V1](/licence.CeCILL-B_V1-fr).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).
