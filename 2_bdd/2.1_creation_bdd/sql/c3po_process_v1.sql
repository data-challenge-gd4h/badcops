\encoding UTF8;

-- Paramètres pour les répertoires des fichiers CSV
\set chemin_bnvd_substance :chemin_bnvd_substance
\set chemin_bnvd_ref_classification :chemin_bnvd_ref_classification
\set chemin_bnvd_substance_classification :chemin_bnvd_substance_classification
\set chemin_bnvd_amm :chemin_bnvd_amm
\set chemin_bnvd_amm_evol_composition :chemin_bnvd_amm_evol_composition
\set chemin_bnvd_pcp :chemin_bnvd_pcp
\set chemin_bnvd_pcp_evol_composition :chemin_bnvd_pcp_evol_composition
\set chemin_ephy_substance :chemin_ephy_substance
\set chemin_ephy_substance_bnvd :chemin_ephy_substance_bnvd
\set chemin_ephy_substance_variants :chemin_ephy_substance_variants
\set chemin_ephy_amm :chemin_ephy_amm
\set chemin_ephy_amm_bnvd :chemin_ephy_amm_bnvd
\set chemin_ephy_pcp :chemin_ephy_pcp
\set chemin_ephy_pcp_origine :chemin_ephy_pcp_origine
\set chemin_ephy_pcp_bnvd :chemin_ephy_pcp_bnvd
\set chemin_ephy_amm_second_nom :chemin_ephy_amm_second_nom
\set chemin_ephy_amm_composition :chemin_ephy_amm_composition
\set chemin_ephy_amm_usage :chemin_ephy_amm_usage
\set chemin_ephy_ref_culture_rpg :chemin_ephy_ref_culture_rpg
\set chemin_ephy_usage_culture :chemin_ephy_usage_culture
\set chemin_ephy_amm_mention_danger :chemin_ephy_amm_mention_danger
\set chemin_ephy_amm_phrase_risque :chemin_ephy_amm_phrase_risque
\set chemin_agritox_substance :chemin_agritox_substance
\set chemin_agritox_substance_bnvd :chemin_agritox_substance_bnvd
\set chemin_agritox_substance_classement :chemin_agritox_substance_classement
\set chemin_agritox_substance_proprietes_pc :chemin_agritox_substance_proprietes_pc
\set chemin_agritox_substance_toxicite :chemin_agritox_substance_toxicite
\set chemin_agritox_substance_ecotoxicite :chemin_agritox_substance_ecotoxicite
\set chemin_agritox_substance_vtr :chemin_agritox_substance_vtr
\set chemin_atp_substance :chemin_atp_substance
\set chemin_atp_substance_bnvd :chemin_atp_substance_bnvd
\set chemin_atp_substance_classement :chemin_atp_substance_classement
\set chemin_eupdb_substance :chemin_eupdb_substance
\set chemin_eupdb_substance_bnvd :chemin_eupdb_substance_bnvd
\set chemin_eupdb_substance_statut :chemin_eupdb_substance_statut
\set chemin_eupdb_substance_categorie :chemin_eupdb_substance_categorie
\set chemin_eupdb_substance_toxicite :chemin_eupdb_substance_toxicite
\set chemin_eupdb_substance_lmr :chemin_eupdb_substance_lmr
\set chemin_sandre_parametres :chemin_sandre_parametres
\set chemin_sandre_substance_bnvd :chemin_sandre_substance_bnvd
\set chemin_sandre_gpes_parametres :chemin_sandre_gpes_parametres
\set chemin_sandre_parametres_gpes_parametres :chemin_sandre_parametres_gpes_parametres
\set chemin_c3po_source :chemin_c3po_source
\set chemin_c3po_version :chemin_c3po_version


-- Intégration des données dans les tables
COPY bnvd.substance FROM :'chemin_bnvd_substance' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY bnvd.ref_classification FROM :'chemin_bnvd_ref_classification' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY bnvd.substance_classification FROM :'chemin_bnvd_substance_classification' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY bnvd.amm FROM :'chemin_bnvd_amm' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY bnvd.amm_evol_composition FROM :'chemin_bnvd_amm_evol_composition' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY bnvd.pcp FROM :'chemin_bnvd_pcp' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY bnvd.pcp_evol_composition FROM :'chemin_bnvd_pcp_evol_composition' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.substance FROM :'chemin_ephy_substance' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.substance_bnvd FROM :'chemin_ephy_substance_bnvd' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.substance_variants FROM :'chemin_ephy_substance_variants' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.amm FROM :'chemin_ephy_amm' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.amm_bnvd FROM :'chemin_ephy_amm_bnvd' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.pcp FROM :'chemin_ephy_pcp' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.pcp_origine FROM :'chemin_ephy_pcp_origine' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.pcp_bnvd FROM :'chemin_ephy_pcp_bnvd' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.amm_second_nom FROM :'chemin_ephy_amm_second_nom' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.amm_composition FROM :'chemin_ephy_amm_composition' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.amm_usage FROM :'chemin_ephy_amm_usage' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.ref_culture_rpg FROM :'chemin_ephy_ref_culture_rpg' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.usage_culture FROM :'chemin_ephy_usage_culture' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.amm_mention_danger FROM :'chemin_ephy_amm_mention_danger' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY ephy.amm_phrase_risque FROM :'chemin_ephy_amm_phrase_risque' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY agritox.substance FROM :'chemin_agritox_substance' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY agritox.substance_bnvd FROM :'chemin_agritox_substance_bnvd' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY agritox.substance_classement FROM :'chemin_agritox_substance_classement' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY agritox.substance_proprietes_pc FROM :'chemin_agritox_substance_proprietes_pc' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY agritox.substance_toxicite FROM :'chemin_agritox_substance_toxicite' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY agritox.substance_ecotoxicite FROM :'chemin_agritox_substance_ecotoxicite' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY agritox.substance_vtr FROM :'chemin_agritox_substance_vtr' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY atp.substance FROM :'chemin_atp_substance' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY atp.substance_bnvd FROM :'chemin_atp_substance_bnvd' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY atp.substance_classement FROM :'chemin_atp_substance_classement' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY eupdb.substance FROM :'chemin_eupdb_substance' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY eupdb.substance_bnvd FROM :'chemin_eupdb_substance_bnvd' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY eupdb.substance_statut FROM :'chemin_eupdb_substance_statut' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY eupdb.substance_categorie FROM :'chemin_eupdb_substance_categorie' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY eupdb.substance_toxicite FROM :'chemin_eupdb_substance_toxicite' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY eupdb.substance_lmr FROM :'chemin_eupdb_substance_lmr' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY sandre.parametres FROM :'chemin_sandre_parametres' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY sandre.substance_bnvd FROM :'chemin_sandre_substance_bnvd' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY sandre.gpes_parametres FROM :'chemin_sandre_gpes_parametres' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY sandre.parametres_gpes_parametres FROM :'chemin_sandre_parametres_gpes_parametres' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY c3po.source FROM :'chemin_c3po_source' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';
COPY c3po.version FROM :'chemin_c3po_version' WITH CSV HEADER DELIMITER ';' QUOTE '"' ENCODING 'UTF8';

