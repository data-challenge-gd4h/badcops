\encoding UTF8;

-- Paramètre pour le nom de la base de données
\set dbname :dbname

---
--- Création de la base de données V1
---
DROP DATABASE IF EXISTS :dbname;
CREATE DATABASE :dbname;

---
--- Utilisation de la base de données
---
\c :dbname

---
--- Création des schémas
---
DROP SCHEMA IF EXISTS bnvd CASCADE;
DROP SCHEMA IF EXISTS ephy CASCADE;
DROP SCHEMA IF EXISTS agritox CASCADE;
DROP SCHEMA IF EXISTS atp CASCADE;
DROP SCHEMA IF EXISTS eupdb CASCADE;
DROP SCHEMA IF EXISTS sandre CASCADE;
DROP SCHEMA IF EXISTS c3po CASCADE;

CREATE SCHEMA bnvd;
CREATE SCHEMA ephy;
CREATE SCHEMA agritox;
CREATE SCHEMA atp;
CREATE SCHEMA eupdb;
CREATE SCHEMA sandre;
CREATE SCHEMA c3po;

---
--- Création des tables
---

CREATE TABLE bnvd.substance (id_substance TEXT NOT NULL,
substance_cas TEXT NOT NULL,
substance TEXT NOT NULL,
cas TEXT,
fonction_principale TEXT NOT NULL,
fonction_precision TEXT,
fonction_autres TEXT,
annee_min INTEGER NOT NULL,
annee_max INTEGER NOT NULL,
rpd_classification_last TEXT NOT NULL,
rpd_mention_last TEXT,
rpd_taux_last REAL NOT NULL,
CONSTRAINT substance_pk PRIMARY KEY (id_substance)
) WITH (OIDS=FALSE);

CREATE TABLE bnvd.ref_classification (id_classif TEXT NOT NULL,
annee INTEGER NOT NULL,
rpd_classification TEXT NOT NULL,
rpd_mention TEXT,
rpd_classification_taux REAL NOT NULL,
rpd_mention_taux REAL,
rpd_taux REAL NOT NULL,
CONSTRAINT ref_classification_pk PRIMARY KEY (id_classif)
) WITH (OIDS=FALSE);

CREATE TABLE bnvd.substance_classification (id_substance TEXT,
id_classif TEXT
) WITH (OIDS=FALSE);

CREATE TABLE bnvd.amm (amm TEXT NOT NULL,
annee_min INTEGER NOT NULL,
annee_max INTEGER NOT NULL,
conditionnement TEXT NOT NULL,
CONSTRAINT amm_pk PRIMARY KEY (amm)
) WITH (OIDS=FALSE);

CREATE TABLE bnvd.amm_evol_composition (amm TEXT NOT NULL,
annee INTEGER NOT NULL,
eaj TEXT NOT NULL,
id_substance TEXT NOT NULL
) WITH (OIDS=FALSE);

CREATE TABLE bnvd.pcp (pcp TEXT NOT NULL,
annee_min INTEGER NOT NULL,
annee_max INTEGER NOT NULL,
conditionnement TEXT NOT NULL,
CONSTRAINT pcp_pk PRIMARY KEY (pcp)
) WITH (OIDS=FALSE);

CREATE TABLE bnvd.pcp_evol_composition (pcp TEXT NOT NULL,
annee INTEGER NOT NULL,
eaj TEXT NOT NULL,
id_substance TEXT NOT NULL
) WITH (OIDS=FALSE);

CREATE TABLE ephy.substance (id_substance TEXT NOT NULL,
substance_cas_variant TEXT,
substance TEXT,
cas TEXT,
variant TEXT,
etat_autorisation TEXT,
CONSTRAINT substance_pk PRIMARY KEY (id_substance)
) WITH (OIDS=FALSE);

CREATE TABLE ephy.substance_bnvd (id_substance_bnvd TEXT,
id_substance_ephy TEXT
) WITH (OIDS=FALSE);

CREATE TABLE ephy.substance_variants (id_substance TEXT,
variant_exp TEXT,
cas_variant TEXT
) WITH (OIDS=FALSE);

CREATE TABLE ephy.amm (amm TEXT NOT NULL,
nom_produit TEXT,
titulaire TEXT,
type_commercial TEXT,
type_produit TEXT,
etat_autorisation TEXT,
date_retrait TIMESTAMP,
date_premiere_autorisation TIMESTAMP,
produit_ref_amm TEXT,
produit_ref_nom TEXT,
CONSTRAINT amm_pk PRIMARY KEY (amm)
) WITH (OIDS=FALSE);

CREATE TABLE ephy.amm_bnvd (amm_bnvd TEXT,
amm_ephy TEXT
) WITH (OIDS=FALSE);

CREATE TABLE ephy.pcp (pcp TEXT NOT NULL,
nom_produit TEXT,
detenteur_pcp TEXT,
type_commercial TEXT,
type_produit TEXT,
etat_autorisation TEXT,
produit_ref_amm TEXT,
produit_ref_nom TEXT,
CONSTRAINT pcp_pk PRIMARY KEY (pcp)
) WITH (OIDS=FALSE);

CREATE TABLE ephy.pcp_origine (pcp TEXT,
amm_produit_importe TEXT,
nom_produit_importe TEXT,
etat_membre_origine TEXT
) WITH (OIDS=FALSE);

CREATE TABLE ephy.pcp_bnvd (pcp_bnvd TEXT,
pcp_ephy TEXT
) WITH (OIDS=FALSE);

CREATE TABLE ephy.amm_second_nom (amm TEXT,
second_nom_produit TEXT
) WITH (OIDS=FALSE);

CREATE TABLE ephy.amm_composition (amm TEXT,
id_substance TEXT,
variant_exp TEXT,
teneur_substance REAL,
teneur_substance_unite TEXT
) WITH (OIDS=FALSE);

CREATE TABLE ephy.amm_usage (id_usage TEXT NOT NULL,
amm TEXT,
usage_gamme TEXT,
mentions_autorisees TEXT,
usage_restrictions_court TEXT,
usage_restrictions_long TEXT,
fonctions TEXT,
formulations TEXT,
usage TEXT,
usage_date_decision TIMESTAMP,
stade_cultural_min_bbch INTEGER,
stade_cultural_max_bbch INTEGER,
usage_etat TEXT,
dose_homologuee REAL,
dose_homologuee_unite TEXT,
dar_jours INTEGER,
dar_bbch INTEGER,
nb_max_applications INTEGER,
usage_date_fin_distribution TIMESTAMP,
usage_date_fin_utilisation TIMESTAMP,
conditions_emploi TEXT,
znt_aquatique REAL,
znt_arthropodes_non_cibles REAL,
znt_plantes_non_cibles REAL,
intervalle_min INTEGER,
CONSTRAINT amm_usage_pk PRIMARY KEY (id_usage)
) WITH (OIDS=FALSE);

CREATE TABLE ephy.ref_culture_rpg (id_culture TEXT NOT NULL,
culture TEXT,
code_gpe_cult_rpg TEXT,
libelle_gpe_cult_rpg TEXT,
CONSTRAINT ref_culture_rpg_pk PRIMARY KEY (id_culture)
) WITH (OIDS=FALSE);

CREATE TABLE ephy.usage_culture (id_usage TEXT,
id_culture TEXT
) WITH (OIDS=FALSE);

CREATE TABLE ephy.amm_mention_danger (amm TEXT,
classes_mention_danger_lb_court TEXT,
classes_mention_danger_lb_long TEXT
) WITH (OIDS=FALSE);

CREATE TABLE ephy.amm_phrase_risque (amm TEXT,
phrases_risque_lb_court TEXT,
phrases_risque_lb_long TEXT
) WITH (OIDS=FALSE);

CREATE TABLE agritox.substance (id_substance TEXT NOT NULL,
substance_cas TEXT,
substance TEXT,
cas TEXT,
nom_uipac TEXT,
ec TEXT,
masse_molaire TEXT,
formule_brute TEXT,
purete_minimale TEXT,
impuretes_pertinentes TEXT,
CONSTRAINT substance_pk PRIMARY KEY (id_substance)
) WITH (OIDS=FALSE);

CREATE TABLE agritox.substance_bnvd (id_substance_bnvd TEXT,
id_substance_agritox TEXT
) WITH (OIDS=FALSE);

CREATE TABLE agritox.substance_classement (id_substance TEXT NOT NULL,
reference TEXT,
date TIMESTAMP,
code_h TEXT,
categorie_danger TEXT,
mention_danger TEXT,
facteur_m_valeur INTEGER,
facteur_m_origine TEXT,
facteur_m_date TIMESTAMP,
classification_simplifiee TEXT
) WITH (OIDS=FALSE);


CREATE TABLE agritox.substance_proprietes_pc (id_substance TEXT NOT NULL,
propriete TEXT,
temperature TEXT,
ph TEXT,
observation TEXT,
valeur TEXT,
unite TEXT
) WITH (OIDS=FALSE);

CREATE TABLE agritox.substance_toxicite (id_substance TEXT NOT NULL,
nature_etude TEXT,
dl50_cl50 TEXT,
espece TEXT,
sexe TEXT,
valeur TEXT,
unite TEXT
) WITH (OIDS=FALSE);

CREATE TABLE agritox.substance_ecotoxicite (id_substance TEXT NOT NULL,
etudes TEXT,
donnee_toxicite TEXT,
donnee_toxicite_valeur TEXT,
donnee_toxicite_unite TEXT,
facteur_securite TEXT,
valeur_pnec TEXT,
unite_pnec TEXT
) WITH (OIDS=FALSE);

CREATE TABLE agritox.substance_vtr (id_substance TEXT NOT NULL,
nom TEXT,
valeur TEXT,
unite TEXT,
source TEXT,
date TIMESTAMP,
etude_pivot_1 TEXT,
etude_pivot_1_noael TEXT,
etude_pivot_1_noael_unite TEXT,
etude_pivot_2 TEXT,
facteur_securite TEXT,
facteur_correction_abs_orale_incompl TEXT
) WITH (OIDS=FALSE);

CREATE TABLE atp.substance (id_substance TEXT NOT NULL,
index TEXT,
ici TEXT,
ec TEXT,
cas TEXT,
CONSTRAINT substance_pk PRIMARY KEY (id_substance)
) WITH (OIDS=FALSE);

CREATE TABLE atp.substance_bnvd (id_substance_bnvd TEXT,
id_substance_atp TEXT
) WITH (OIDS=FALSE);

CREATE TABLE atp.substance_classement (id_substance TEXT NOT NULL,
code_h_detail TEXT,
code_h TEXT,
categorie_danger TEXT,
mention_danger TEXT,
classification_simplifiee TEXT
) WITH (OIDS=FALSE);

CREATE TABLE eupdb.substance (id_substance TEXT NOT NULL,
id_interne TEXT NOT NULL,
substance TEXT NOT NULL,
cas TEXT,
cas_modif TEXT,
gpe_substance TEXT,
CONSTRAINT substance_pk PRIMARY KEY (id_substance)
) WITH (OIDS=FALSE);

CREATE TABLE eupdb.substance_statut (id_substance TEXT NOT NULL,
etat_reg_1107_2009 TEXT,
date_approbation TIMESTAMP,
date_expiration_approbation TIMESTAMP,
evaluation_risque_par TEXT,
em_rapporteur TEXT,
co_em_rapporteur TEXT,
legislations_anciennes TEXT,
legislations_actives TEXT,
autorisations_pays TEXT,
autorisation_france TEXT
) WITH (OIDS=FALSE);

CREATE TABLE eupdb.substance_categorie (id_substance TEXT NOT NULL,
fonction TEXT,
micro_organisme TEXT,
substance_base TEXT,
faible_risque TEXT,
candidat_substitution TEXT,
candidat_substitution_type TEXT
) WITH (OIDS=FALSE);

CREATE TABLE eupdb.substance_toxicite (id_substance TEXT NOT NULL,
tox_dja_valeur TEXT,
tox_dja_source TEXT,
tox_dja_remarque TEXT,
tox_drfa_valeur TEXT,
tox_drfa_source TEXT,
tox_drfa_remarque TEXT,
tox_aoel_valeur TEXT,
tox_aoel_source TEXT,
tox_aoel_remarque TEXT,
tox_aaoel_valeur TEXT,
tox_aaoel_source TEXT,
tox_aaoel_remarque TEXT,
tox_autre_valeur TEXT,
tox_autre_source TEXT,
tox_autre_remarque TEXT
) WITH (OIDS=FALSE);

CREATE TABLE eupdb.substance_lmr (id_substance TEXT NOT NULL,
pesticide_residue_linked TEXT,
pest_res_linked_legislation TEXT,
pest_res_linked_legislation_url TEXT,
pest_res_linked_annex TEXT,
pest_res_mrl_webpage TEXT
) WITH (OIDS=FALSE);

CREATE TABLE eupdb.substance_bnvd (id_substance_bnvd TEXT NOT NULL,
id_substance_eupdb TEXT
) WITH (OIDS=FALSE);

CREATE TABLE sandre.parametres (cdparametre TEXT NOT NULL,
nomparametre TEXT,
cas TEXT,
CONSTRAINT parametres_pk PRIMARY KEY (cdparametre)
) WITH (OIDS=FALSE);

CREATE TABLE sandre.substance_bnvd (id_substance_bnvd TEXT,
cdparametre TEXT
) WITH (OIDS=FALSE);

CREATE TABLE sandre.gpes_parametres (cdgroupeparametres TEXT NOT NULL,
nomgroupeparametres TEXT,
famille TEXT,
fonction TEXT,
CONSTRAINT gpes_parametres_pk PRIMARY KEY (cdgroupeparametres)
) WITH (OIDS=FALSE);

CREATE TABLE sandre.parametres_gpes_parametres (cdparametre TEXT,
cdgroupeparametres TEXT
) WITH (OIDS=FALSE);

CREATE TABLE c3po.source (source TEXT NOT NULL,
date_publication TIMESTAMP,
date_collecte TIMESTAMP NOT NULL,
version TEXT
) WITH (OIDS=FALSE);

CREATE TABLE c3po.version (version TEXT NOT NULL,
date_publication TIMESTAMP NOT NULL
) WITH (OIDS=FALSE);

