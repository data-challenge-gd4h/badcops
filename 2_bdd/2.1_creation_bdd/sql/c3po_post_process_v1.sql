\encoding UTF8;

---
--- Ajout des clés étrangères
---

ALTER TABLE bnvd.substance_classification ADD CONSTRAINT substance_classification_fk0 FOREIGN KEY (id_substance) REFERENCES bnvd.substance (id_substance);
ALTER TABLE bnvd.substance_classification ADD CONSTRAINT substance_classification_fk1 FOREIGN KEY (id_classif) REFERENCES bnvd.ref_classification (id_classif);
ALTER TABLE bnvd.amm_evol_composition ADD CONSTRAINT amm_evol_composition_fk0 FOREIGN KEY (amm) REFERENCES bnvd.amm (amm);
ALTER TABLE bnvd.amm_evol_composition ADD CONSTRAINT amm_evol_composition_fk1 FOREIGN KEY (id_substance) REFERENCES bnvd.substance (id_substance);
ALTER TABLE bnvd.pcp_evol_composition ADD CONSTRAINT pcp_evol_composition_fk0 FOREIGN KEY (pcp) REFERENCES bnvd.pcp (pcp);
ALTER TABLE bnvd.pcp_evol_composition ADD CONSTRAINT pcp_evol_composition_fk1 FOREIGN KEY (id_substance) REFERENCES bnvd.substance (id_substance);
ALTER TABLE ephy.substance_bnvd ADD CONSTRAINT substance_bnvd_fk0 FOREIGN KEY (id_substance_bnvd) REFERENCES bnvd.substance (id_substance);
ALTER TABLE ephy.substance_bnvd ADD CONSTRAINT substance_bnvd_fk1 FOREIGN KEY (id_substance_ephy) REFERENCES ephy.substance (id_substance);
ALTER TABLE ephy.substance_variants ADD CONSTRAINT substance_variants_fk0 FOREIGN KEY (id_substance) REFERENCES ephy.substance (id_substance);
ALTER TABLE ephy.amm_bnvd ADD CONSTRAINT amm_bnvd_fk0 FOREIGN KEY (amm_bnvd) REFERENCES bnvd.amm (amm);
ALTER TABLE ephy.amm_bnvd ADD CONSTRAINT amm_bnvd_fk1 FOREIGN KEY (amm_ephy) REFERENCES ephy.amm (amm);
ALTER TABLE ephy.pcp ADD CONSTRAINT pcp_fk0 FOREIGN KEY (produit_ref_amm) REFERENCES ephy.amm (amm);
ALTER TABLE ephy.pcp_origine ADD CONSTRAINT pcp_origine_fk0 FOREIGN KEY (pcp) REFERENCES ephy.pcp (pcp);
ALTER TABLE ephy.pcp_bnvd ADD CONSTRAINT pcp_bnvd_fk0 FOREIGN KEY (pcp_bnvd) REFERENCES bnvd.pcp (pcp);
ALTER TABLE ephy.pcp_bnvd ADD CONSTRAINT pcp_bnvd_fk1 FOREIGN KEY (pcp_ephy) REFERENCES ephy.pcp (pcp);
ALTER TABLE ephy.amm_second_nom ADD CONSTRAINT amm_second_nom_fk0 FOREIGN KEY (amm) REFERENCES ephy.amm (amm);
ALTER TABLE ephy.amm_composition ADD CONSTRAINT amm_composition_fk0 FOREIGN KEY (amm) REFERENCES ephy.amm (amm);
ALTER TABLE ephy.amm_composition ADD CONSTRAINT amm_composition_fk1 FOREIGN KEY (id_substance) REFERENCES ephy.substance (id_substance);
ALTER TABLE ephy.amm_usage ADD CONSTRAINT amm_usage_fk0 FOREIGN KEY (amm) REFERENCES ephy.amm (amm);
ALTER TABLE ephy.usage_culture ADD CONSTRAINT usage_culture_fk0 FOREIGN KEY (id_usage) REFERENCES ephy.amm_usage (id_usage);
ALTER TABLE ephy.usage_culture ADD CONSTRAINT usage_culture_fk1 FOREIGN KEY (id_culture) REFERENCES ephy.ref_culture_rpg (id_culture);
ALTER TABLE ephy.amm_mention_danger ADD CONSTRAINT amm_mention_danger_fk0 FOREIGN KEY (amm) REFERENCES ephy.amm (amm);
ALTER TABLE ephy.amm_phrase_risque ADD CONSTRAINT amm_phrase_risque_fk0 FOREIGN KEY (amm) REFERENCES ephy.amm (amm);
ALTER TABLE agritox.substance_bnvd ADD CONSTRAINT substance_bnvd_fk2 FOREIGN KEY (id_substance_bnvd) REFERENCES bnvd.substance (id_substance);
ALTER TABLE agritox.substance_bnvd ADD CONSTRAINT substance_bnvd_fk3 FOREIGN KEY (id_substance_agritox) REFERENCES agritox.substance (id_substance);
ALTER TABLE agritox.substance_classement ADD CONSTRAINT substance_classement_fk0 FOREIGN KEY (id_substance) REFERENCES agritox.substance (id_substance);
ALTER TABLE agritox.substance_proprietes_pc ADD CONSTRAINT substance_proprietes_pc_fk0 FOREIGN KEY (id_substance) REFERENCES agritox.substance (id_substance);
ALTER TABLE agritox.substance_toxicite ADD CONSTRAINT substance_toxicite_fk0 FOREIGN KEY (id_substance) REFERENCES agritox.substance (id_substance);
ALTER TABLE agritox.substance_ecotoxicite ADD CONSTRAINT substance_ecotoxicite_fk0 FOREIGN KEY (id_substance) REFERENCES agritox.substance (id_substance);
ALTER TABLE agritox.substance_vtr ADD CONSTRAINT substance_vtr_fk0 FOREIGN KEY (id_substance) REFERENCES agritox.substance (id_substance);
ALTER TABLE atp.substance_bnvd ADD CONSTRAINT substance_bnvd_fk4 FOREIGN KEY (id_substance_bnvd) REFERENCES bnvd.substance (id_substance);
ALTER TABLE atp.substance_bnvd ADD CONSTRAINT substance_bnvd_fk5 FOREIGN KEY (id_substance_atp) REFERENCES atp.substance (id_substance);
ALTER TABLE atp.substance_classement ADD CONSTRAINT substance_classement_fk1 FOREIGN KEY (id_substance) REFERENCES atp.substance (id_substance);
ALTER TABLE eupdb.substance_statut ADD CONSTRAINT substance_statut_fk0 FOREIGN KEY (id_substance) REFERENCES eupdb.substance (id_substance);
ALTER TABLE eupdb.substance_categorie ADD CONSTRAINT substance_categorie_fk0 FOREIGN KEY (id_substance) REFERENCES eupdb.substance (id_substance);
ALTER TABLE eupdb.substance_toxicite ADD CONSTRAINT substance_toxicite_fk1 FOREIGN KEY (id_substance) REFERENCES eupdb.substance (id_substance);
ALTER TABLE eupdb.substance_lmr ADD CONSTRAINT substance_lmr_fk0 FOREIGN KEY (id_substance) REFERENCES eupdb.substance (id_substance);
ALTER TABLE eupdb.substance_bnvd ADD CONSTRAINT substance_bnvd_fk6 FOREIGN KEY (id_substance_bnvd) REFERENCES bnvd.substance (id_substance);
ALTER TABLE eupdb.substance_bnvd ADD CONSTRAINT substance_bnvd_fk7 FOREIGN KEY (id_substance_eupdb) REFERENCES eupdb.substance (id_substance);
ALTER TABLE sandre.substance_bnvd ADD CONSTRAINT substance_bnvd_fk8 FOREIGN KEY (id_substance_bnvd) REFERENCES bnvd.substance (id_substance);
ALTER TABLE sandre.substance_bnvd ADD CONSTRAINT substance_bnvd_fk9 FOREIGN KEY (cdparametre) REFERENCES sandre.parametres (cdparametre);
ALTER TABLE sandre.parametres_gpes_parametres ADD CONSTRAINT parametres_gpes_parametres_fk0 FOREIGN KEY (cdparametre) REFERENCES sandre.parametres (cdparametre);
ALTER TABLE sandre.parametres_gpes_parametres ADD CONSTRAINT parametres_gpes_parametres_fk1 FOREIGN KEY (cdgroupeparametres) REFERENCES sandre.gpes_parametres (cdgroupeparametres);
