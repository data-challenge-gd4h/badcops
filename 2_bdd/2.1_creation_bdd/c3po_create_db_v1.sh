#!/bin/bash
# -*- coding: utf-8 -*-

echo "Bienvenue dans le script d'intégration des données C3PO."

# Définir des valeurs par défaut
hote_default="localhost"
port_default="5432"
utilisateur_default="postgres"
dbname_default="c3po_v1"

# Lire les entrées avec des valeurs par défaut
echo "Vous allez être invité à spécifier les paramètres de configuration de la base de données."
echo "Pour tous ces paramètres (sauf mot de passe à saisir) :"
echo "	- Appuyer directement sur Entrée pour conserver les valeurs par défaut"
echo "	- Saisir la valeur souhaitée sinon"

# Demander à l'utilisateur s'il souhaite spécifier le répertoire bin
read -r -p "Voulez-vous spécifier le répertoire bin de PostgreSQL ? (O/N) " reponse_repertoire_psql

if [[ $reponse_repertoire_psql =~ ^[Oo]$ ]]; then
	# Définir une valeur par défaut pour le répertoire bin
	repertoire_psql_default="/c/Program Files/PostgreSQL/14/bin"
    # Lire l'entrée pour le répertoire bin avec une valeur par défaut
    read -p "Entrez le répertoire bin de PostgreSQL (valeur par défaut : $repertoire_psql_default): " repertoire_psql
    repertoire_psql=${repertoire_psql:-$repertoire_psql_default}
	# Ajouter le répertoire bin de PostgreSQL à la variable PATH
	export PATH="$repertoire_psql:$PATH"
	echo
fi

# Hote
read -p "Entrez l'hôte (valeur par défaut : $hote_default) : " hote
hote=${hote:-$hote_default}
echo "Hôte : $hote"

# Port
read -p "Entrez le port (valeur par défaut : $port_default) : " port
port=${port:-$port_default}
echo "Port : $port"

# Nom utilisateur
read -p "Entrez le nom d'utilisateur (valeur par défaut : $utilisateur_default) : " utilisateur
utilisateur=${utilisateur:-$utilisateur_default}
echo "Utilisateur : $utilisateur"

# Mot de passe
read -s -p "Entrez le mot de passe : " mot_de_passe
echo

# Base de données C3PO
read -p "Entrez le nom de la version de la base de donnée C3PO (valeur par défaut : $dbname_default): " dbname
dbname=${dbname:-$dbname_default}
echo "Nom de la base de données : $dbname"

# Récupérer le chemin du répertoire actuel du script
repertoire_script="$(dirname "$(realpath "$0")")"

# Modifier les chemins des fichiers CSV pour qu'ils soient interprétés par Bash
chemin_bnvd_substance="$repertoire_script/../../1_notebook/1.1_bnvd/output/substance.csv"
chemin_bnvd_ref_classification="$repertoire_script/../../1_notebook/1.1_bnvd/output/ref_classification.csv"
chemin_bnvd_substance_classification="$repertoire_script/../../1_notebook/1.1_bnvd/output/substance_classification.csv"
chemin_bnvd_amm="$repertoire_script/../../1_notebook/1.1_bnvd/output/amm.csv"
chemin_bnvd_amm_evol_composition="$repertoire_script/../../1_notebook/1.1_bnvd/output/amm_evol_composition.csv"
chemin_bnvd_pcp="$repertoire_script/../../1_notebook/1.1_bnvd/output/pcp.csv"
chemin_bnvd_pcp_evol_composition="$repertoire_script/../../1_notebook/1.1_bnvd/output/pcp_evol_composition.csv"
chemin_ephy_substance="$repertoire_script/../../1_notebook/1.2_ephy/output/substance.csv"
chemin_ephy_substance_bnvd="$repertoire_script/../../1_notebook/1.2_ephy/output/substance_bnvd.csv"
chemin_ephy_substance_variants="$repertoire_script/../../1_notebook/1.2_ephy/output/substance_variants.csv"
chemin_ephy_amm="$repertoire_script/../../1_notebook/1.2_ephy/output/amm.csv"
chemin_ephy_amm_bnvd="$repertoire_script/../../1_notebook/1.2_ephy/output/amm_bnvd.csv"
chemin_ephy_pcp="$repertoire_script/../../1_notebook/1.2_ephy/output/pcp.csv"
chemin_ephy_pcp_origine="$repertoire_script/../../1_notebook/1.2_ephy/output/pcp_origine.csv"
chemin_ephy_pcp_bnvd="$repertoire_script/../../1_notebook/1.2_ephy/output/pcp_bnvd.csv"
chemin_ephy_amm_second_nom="$repertoire_script/../../1_notebook/1.2_ephy/output/amm_second_nom.csv"
chemin_ephy_amm_composition="$repertoire_script/../../1_notebook/1.2_ephy/output/amm_composition.csv"
chemin_ephy_amm_usage="$repertoire_script/../../1_notebook/1.2_ephy/output/amm_usage.csv"
chemin_ephy_ref_culture_rpg="$repertoire_script/../../1_notebook/1.2_ephy/output/ref_culture_rpg.csv"
chemin_ephy_usage_culture="$repertoire_script/../../1_notebook/1.2_ephy/output/usage_culture.csv"
chemin_ephy_amm_mention_danger="$repertoire_script/../../1_notebook/1.2_ephy/output/amm_mention_danger.csv"
chemin_ephy_amm_phrase_risque="$repertoire_script/../../1_notebook/1.2_ephy/output/amm_phrase_risque.csv"
chemin_agritox_substance="$repertoire_script/../../1_notebook/1.3_agritox/output/substance.csv"
chemin_agritox_substance_bnvd="$repertoire_script/../../1_notebook/1.3_agritox/output/substance_bnvd.csv"
chemin_agritox_substance_classement="$repertoire_script/../../1_notebook/1.3_agritox/output/substance_classement.csv"
chemin_agritox_substance_proprietes_pc="$repertoire_script/../../1_notebook/1.3_agritox/output/substance_proprietes_pc.csv"
chemin_agritox_substance_toxicite="$repertoire_script/../../1_notebook/1.3_agritox/output/substance_toxicite.csv"
chemin_agritox_substance_ecotoxicite="$repertoire_script/../../1_notebook/1.3_agritox/output/substance_ecotoxicite.csv"
chemin_agritox_substance_vtr="$repertoire_script/../../1_notebook/1.3_agritox/output/substance_vtr.csv"
chemin_atp_substance="$repertoire_script/../../1_notebook/1.4_atp/output/substance.csv"
chemin_atp_substance_bnvd="$repertoire_script/../../1_notebook/1.4_atp/output/substance_bnvd.csv"
chemin_atp_substance_classement="$repertoire_script/../../1_notebook/1.4_atp/output/substance_classement.csv"
chemin_eupdb_substance="$repertoire_script/../../1_notebook/1.5_eupdb/output/substance.csv"
chemin_eupdb_substance_bnvd="$repertoire_script/../../1_notebook/1.5_eupdb/output/substance_bnvd.csv"
chemin_eupdb_substance_statut="$repertoire_script/../../1_notebook/1.5_eupdb/output/substance_statut.csv"
chemin_eupdb_substance_categorie="$repertoire_script/../../1_notebook/1.5_eupdb/output/substance_categorie.csv"
chemin_eupdb_substance_toxicite="$repertoire_script/../../1_notebook/1.5_eupdb/output/substance_toxicite.csv"
chemin_eupdb_substance_lmr="$repertoire_script/../../1_notebook/1.5_eupdb/output/substance_lmr.csv"
chemin_sandre_parametres="$repertoire_script/../../1_notebook/1.6_sandre/output/parametres.csv"
chemin_sandre_substance_bnvd="$repertoire_script/../../1_notebook/1.6_sandre/output/substance_bnvd.csv"
chemin_sandre_gpes_parametres="$repertoire_script/../../1_notebook/1.6_sandre/output/gpes_parametres.csv"
chemin_sandre_parametres_gpes_parametres="$repertoire_script/../../1_notebook/1.6_sandre/output/parametres_gpes_parametres.csv"
chemin_c3po_source="$repertoire_script/../../1_notebook/1.7_c3po/source.csv"
chemin_c3po_version="$repertoire_script/../../1_notebook/1.7_c3po/version.csv"

chemin_bnvd_substance="$(realpath "$chemin_bnvd_substance")"
chemin_bnvd_ref_classification="$(realpath "$chemin_bnvd_ref_classification")"
chemin_bnvd_substance_classification="$(realpath "$chemin_bnvd_substance_classification")"
chemin_bnvd_amm="$(realpath "$chemin_bnvd_amm")"
chemin_bnvd_amm_evol_composition="$(realpath "$chemin_bnvd_amm_evol_composition")"
chemin_bnvd_pcp="$(realpath "$chemin_bnvd_pcp")"
chemin_bnvd_pcp_evol_composition="$(realpath "$chemin_bnvd_pcp_evol_composition")"
chemin_ephy_substance="$(realpath "$chemin_ephy_substance")"
chemin_ephy_substance_bnvd="$(realpath "$chemin_ephy_substance_bnvd")"
chemin_ephy_substance_variants="$(realpath "$chemin_ephy_substance_variants")"
chemin_ephy_amm="$(realpath "$chemin_ephy_amm")"
chemin_ephy_amm_bnvd="$(realpath "$chemin_ephy_amm_bnvd")"
chemin_ephy_pcp="$(realpath "$chemin_ephy_pcp")"
chemin_ephy_pcp_origine="$(realpath "$chemin_ephy_pcp_origine")"
chemin_ephy_pcp_bnvd="$(realpath "$chemin_ephy_pcp_bnvd")"
chemin_ephy_amm_second_nom="$(realpath "$chemin_ephy_amm_second_nom")"
chemin_ephy_amm_composition="$(realpath "$chemin_ephy_amm_composition")"
chemin_ephy_amm_usage="$(realpath "$chemin_ephy_amm_usage")"
chemin_ephy_ref_culture_rpg="$(realpath "$chemin_ephy_ref_culture_rpg")"
chemin_ephy_usage_culture="$(realpath "$chemin_ephy_usage_culture")"
chemin_ephy_amm_mention_danger="$(realpath "$chemin_ephy_amm_mention_danger")"
chemin_ephy_amm_phrase_risque="$(realpath "$chemin_ephy_amm_phrase_risque")"
chemin_agritox_substance="$(realpath "$chemin_agritox_substance")"
chemin_agritox_substance_bnvd="$(realpath "$chemin_agritox_substance_bnvd")"
chemin_agritox_substance_classement="$(realpath "$chemin_agritox_substance_classement")"
chemin_agritox_substance_proprietes_pc="$(realpath "$chemin_agritox_substance_proprietes_pc")"
chemin_agritox_substance_toxicite="$(realpath "$chemin_agritox_substance_toxicite")"
chemin_agritox_substance_ecotoxicite="$(realpath "$chemin_agritox_substance_ecotoxicite")"
chemin_agritox_substance_vtr="$(realpath "$chemin_agritox_substance_vtr")"
chemin_atp_substance="$(realpath "$chemin_atp_substance")"
chemin_atp_substance_bnvd="$(realpath "$chemin_atp_substance_bnvd")"
chemin_atp_substance_classement="$(realpath "$chemin_atp_substance_classement")"
chemin_eupdb_substance="$(realpath "$chemin_eupdb_substance")"
chemin_eupdb_substance_bnvd="$(realpath "$chemin_eupdb_substance_bnvd")"
chemin_eupdb_substance_statut="$(realpath "$chemin_eupdb_substance_statut")"
chemin_eupdb_substance_categorie="$(realpath "$chemin_eupdb_substance_categorie")"
chemin_eupdb_substance_toxicite="$(realpath "$chemin_eupdb_substance_toxicite")"
chemin_eupdb_substance_lmr="$(realpath "$chemin_eupdb_substance_lmr")"
chemin_sandre_parametres="$(realpath "$chemin_sandre_parametres")"
chemin_sandre_substance_bnvd="$(realpath "$chemin_sandre_substance_bnvd")"
chemin_sandre_gpes_parametres="$(realpath "$chemin_sandre_gpes_parametres")"
chemin_sandre_parametres_gpes_parametres="$(realpath "$chemin_sandre_parametres_gpes_parametres")"
chemin_c3po_source="$(realpath "$chemin_c3po_source")"
chemin_c3po_version="$(realpath "$chemin_c3po_version")"

# Exécuter le script SQL en utilisant le chemin complet
PGPASSWORD="$mot_de_passe" psql -h "$hote" -d postgres -U "$utilisateur" -p "$port" \
  -v dbname="$dbname" \
  -a -f "$repertoire_script/sql/c3po_pre_process_v1.sql"

# Si une erreur se produit, afficher un message approprié
if [ $? -eq 0 ]; then
    echo "Initialisation de la base de données C3PO réussie."
else
    echo "Erreur lors de l'initialisation de la base de données."
	echo "Veuillez vérifier les informations saisies et relancer le script."
	# Ajouter une pause pour empêcher la fermeture automatique
	read -p "Appuyez sur Entrée pour quitter..."
	exit 0
fi

# Exécuter les commandes psql en utilisant les chemins modifiés
PGPASSWORD="$mot_de_passe" psql -h "$hote" -d "$dbname" -U "$utilisateur" -p "$port" \
  -v chemin_bnvd_substance="$chemin_bnvd_substance" \
  -v chemin_bnvd_ref_classification="$chemin_bnvd_ref_classification" \
  -v chemin_bnvd_substance_classification="$chemin_bnvd_substance_classification" \
  -v chemin_bnvd_amm="$chemin_bnvd_amm" \
  -v chemin_bnvd_amm_evol_composition="$chemin_bnvd_amm_evol_composition" \
  -v chemin_bnvd_pcp="$chemin_bnvd_pcp" \
  -v chemin_bnvd_pcp_evol_composition="$chemin_bnvd_pcp_evol_composition" \
  -v chemin_ephy_substance="$chemin_ephy_substance" \
  -v chemin_ephy_substance_bnvd="$chemin_ephy_substance_bnvd" \
  -v chemin_ephy_substance_variants="$chemin_ephy_substance_variants" \
  -v chemin_ephy_amm="$chemin_ephy_amm" \
  -v chemin_ephy_amm_bnvd="$chemin_ephy_amm_bnvd" \
  -v chemin_ephy_pcp="$chemin_ephy_pcp" \
  -v chemin_ephy_pcp_origine="$chemin_ephy_pcp_origine" \
  -v chemin_ephy_pcp_bnvd="$chemin_ephy_pcp_bnvd" \
  -v chemin_ephy_amm_second_nom="$chemin_ephy_amm_second_nom" \
  -v chemin_ephy_amm_composition="$chemin_ephy_amm_composition" \
  -v chemin_ephy_amm_usage="$chemin_ephy_amm_usage" \
  -v chemin_ephy_ref_culture_rpg="$chemin_ephy_ref_culture_rpg" \
  -v chemin_ephy_usage_culture="$chemin_ephy_usage_culture" \
  -v chemin_ephy_amm_mention_danger="$chemin_ephy_amm_mention_danger" \
  -v chemin_ephy_amm_phrase_risque="$chemin_ephy_amm_phrase_risque" \
  -v chemin_agritox_substance="$chemin_agritox_substance" \
  -v chemin_agritox_substance_bnvd="$chemin_agritox_substance_bnvd" \
  -v chemin_agritox_substance_classement="$chemin_agritox_substance_classement" \
  -v chemin_agritox_substance_proprietes_pc="$chemin_agritox_substance_proprietes_pc" \
  -v chemin_agritox_substance_toxicite="$chemin_agritox_substance_toxicite" \
  -v chemin_agritox_substance_ecotoxicite="$chemin_agritox_substance_ecotoxicite" \
  -v chemin_agritox_substance_vtr="$chemin_agritox_substance_vtr" \
  -v chemin_atp_substance="$chemin_atp_substance" \
  -v chemin_atp_substance_bnvd="$chemin_atp_substance_bnvd" \
  -v chemin_atp_substance_classement="$chemin_atp_substance_classement" \
  -v chemin_eupdb_substance="$chemin_eupdb_substance" \
  -v chemin_eupdb_substance_bnvd="$chemin_eupdb_substance_bnvd" \
  -v chemin_eupdb_substance_statut="$chemin_eupdb_substance_statut" \
  -v chemin_eupdb_substance_categorie="$chemin_eupdb_substance_categorie" \
  -v chemin_eupdb_substance_toxicite="$chemin_eupdb_substance_toxicite" \
  -v chemin_eupdb_substance_lmr="$chemin_eupdb_substance_lmr" \
  -v chemin_sandre_parametres="$chemin_sandre_parametres" \
  -v chemin_sandre_substance_bnvd="$chemin_sandre_substance_bnvd" \
  -v chemin_sandre_gpes_parametres="$chemin_sandre_gpes_parametres" \
  -v chemin_sandre_parametres_gpes_parametres="$chemin_sandre_parametres_gpes_parametres" \
  -v chemin_c3po_source="$chemin_c3po_source" \
  -v chemin_c3po_version="$chemin_c3po_version" \
  -a -f "$repertoire_script/sql/c3po_process_v1.sql"

# Si une erreur se produit, afficher un message approprié
if [ $? -eq 0 ]; then
    echo "Intégration des données dans C3PO terminée."
else
    echo "Erreur lors de l'intégration des données dans C3PO."
	echo "Veuillez corriger les éventuelles erreurs et relancer le script."
	# Ajouter une pause pour empêcher la fermeture automatique
	read -p "Appuyez sur Entrée pour quitter..."
	exit 0
fi

# Exécuter les commandes psql en utilisant les chemins modifiés
PGPASSWORD="$mot_de_passe" psql -h "$hote" -d "$dbname" -U "$utilisateur" -p "$port" \
  -a -f "$repertoire_script/sql/c3po_post_process_v1.sql"

# Si une erreur se produit, afficher un message approprié
if [ $? -eq 0 ]; then
    echo "Ajout des clés étrangères dans C3PO terminé."
	# Ajouter une pause pour empêcher la fermeture automatique
	read -p "Appuyez sur Entrée pour quitter..."
else
    echo "Erreur lors de l'ajout des clés étrangères."
	echo "Veuillez corriger les éventuelles erreurs et relancer le script."
	# Ajouter une pause pour empêcher la fermeture automatique
	read -p "Appuyez sur Entrée pour quitter..."
fi
