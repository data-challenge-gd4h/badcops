#!/bin/bash

echo "Bienvenue dans le script de sauvegarde de la base de données C3PO."

# Définir des valeurs par défaut
hote_default="localhost"
port_default="5432"
utilisateur_default="postgres"
nom_dump_default="c3po_v1_dump"
dbname_default="c3po_v1"

# Lire les entrées avec des valeurs par défaut
echo "Vous allez être invité à spécifier les paramètres de configuration de la base de données."
echo "Pour tous ces paramètres (sauf mot de passe à saisir) :"
echo "	- Appuyer directement sur Entrée pour conserver les valeurs par défaut"
echo "	- Saisir la valeur souhaitée sinon"

# Demander à l'utilisateur s'il souhaite spécifier le répertoire bin
read -r -p "Voulez-vous spécifier le répertoire bin de PostgreSQL ? (O/N) " reponse_repertoire_psql

if [[ $reponse_repertoire_psql =~ ^[Oo]$ ]]; then
	# Définir une valeur par défaut pour le répertoire bin
	repertoire_psql_default="/c/Program Files/PostgreSQL/14/bin"
    # Lire l'entrée pour le répertoire bin avec une valeur par défaut
    read -p "Entrez le répertoire bin de PostgreSQL (valeur par défaut : $repertoire_psql_default): " repertoire_psql
    repertoire_psql=${repertoire_psql:-$repertoire_psql_default}
	# Ajouter le répertoire bin de PostgreSQL à la variable PATH
	export PATH="$repertoire_psql:$PATH"
	echo
fi

# Hote
read -p "Entrez l'hôte (valeur par défaut : $hote_default) : " hote
hote=${hote:-$hote_default}
echo "Hôte : $hote"

# Port
read -p "Entrez le port (valeur par défaut : $port_default) : " port
port=${port:-$port_default}
echo "Port : $port"

# Nom utilisateur
read -p "Entrez le nom d'utilisateur (valeur par défaut : $utilisateur_default) : " utilisateur
utilisateur=${utilisateur:-$utilisateur_default}
echo "Utilisateur : $utilisateur"

# Mot de passe
read -s -p "Entrez le mot de passe : " mot_de_passe
echo

# Lister les bases de données contenant 'c3po' dans leur nom
echo "Voici les bases de données trouvées pouvant correspondre à 'c3po' : "
PGPASSWORD="$mot_de_passe" psql -h "$hote" -p "$port" -U "$utilisateur" -t -c "SELECT datname FROM pg_database WHERE datname LIKE '%c3po%';"

# Nom de la base de données
read -p "Entrez le nom de la base de données (valeur par défaut $dbname_default) : " dbname
dbname=${dbname:-$dbname_default}
echo "Nom de la base de données : $dbname"

# Récupérer le chemin du répertoire actuel du script
repertoire_script="$(dirname "$(realpath "$0")")"

# Paramètres de sauvegarde du dump
echo "Le dump va être sauvegardé dans le répertoire : $repertoire_script"
read -p "Entrez le nom du dump (sans spécifier l'extension) qui sera sauvegardé dans ce répertoire (valeur par défaut: $nom_dump_default.sql) :" nom_dump
nom_dump=${nom_dump:-$nom_dump_default}

# Spécification du chemin complet de sauvegarde
chemin_sauvegarde="$repertoire_script/$nom_dump.sql"

# Utiliser pg_dump pour créer le dump de la base de données avec les options pour exporter les clés primaires et étrangères, ainsi que les éventuels index
PGPASSWORD="$mot_de_passe" pg_dump -h "$hote" -p "$port" -U "$utilisateur" -d "$dbname" -f "$chemin_sauvegarde" --inserts --column-inserts

# Si une erreur se produit, afficher un message approprié
if [ $? -eq 0 ]; then
    echo "Connexion à la base de données réussie."
	echo "Le dump de la base de données C3PO a été créé dans : $chemin_sauvegarde"
	# Ajouter une pause pour empêcher la fermeture automatique
	read -p "Appuyez sur Entrée pour quitter..."
else
    echo "Erreur lors de la connexion à la base de données."
	echo "Veuillez vérifier les informations saisies et relancer le script."
	# Ajouter une pause pour empêcher la fermeture automatique
	read -p "Appuyez sur Entrée pour quitter..."
fi
