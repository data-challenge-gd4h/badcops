#!/bin/bash

echo "Bienvenue dans le script de restauration de la base de données C3PO (à partir d'un dump existant)."

# Définir des valeurs par défaut
hote_default="localhost"
port_default="5432"
utilisateur_default="postgres"
nom_dump_default="c3po_v1_dump"
dbname_default="c3po_v1"


# Lire les entrées avec des valeurs par défaut
echo "Vous allez être invité à spécifier les paramètres de configuration de la base de données."
echo "Pour tous ces paramètres (sauf mot de passe à saisir) :"
echo "	- Appuyer directement sur Entrée pour conserver les valeurs par défaut"
echo "	- Saisir la valeur souhaitée sinon"

# Demander à l'utilisateur s'il souhaite spécifier le répertoire bin
read -r -p "Voulez-vous spécifier le répertoire bin de PostgreSQL ? (O/N) " reponse_repertoire_psql

if [[ $reponse_repertoire_psql =~ ^[Oo]$ ]]; then
	# Définir une valeur par défaut pour le répertoire bin
	repertoire_psql_default="/c/Program Files/PostgreSQL/14/bin"
    # Lire l'entrée pour le répertoire bin avec une valeur par défaut
    read -p "Entrez le répertoire bin de PostgreSQL (valeur par défaut : $repertoire_psql_default): " repertoire_psql
    repertoire_psql=${repertoire_psql:-$repertoire_psql_default}
	# Ajouter le répertoire bin de PostgreSQL à la variable PATH
	export PATH="$repertoire_psql:$PATH"
	echo
fi

# Hote
read -p "Entrez l'hôte (valeur par défaut : $hote_default) : " hote
hote=${hote:-$hote_default}
echo "Hôte : $hote"

# Port
read -p "Entrez le port (valeur par défaut : $port_default) : " port
port=${port:-$port_default}
echo "Port : $port"

# Nom utilisateur
read -p "Entrez le nom d'utilisateur (valeur par défaut : $utilisateur_default) : " utilisateur
utilisateur=${utilisateur:-$utilisateur_default}
echo "Utilisateur : $utilisateur"

# Mot de passe
read -s -p "Entrez le mot de passe : " mot_de_passe
echo

# Récupérer le chemin du répertoire actuel du script
repertoire_script="$(dirname "$(realpath "$0")")"

# Paramètres de restauration du dump
echo "Assurez-vous de bien disposer du dump (au format .sql) que vous souhaitez restaurer dans le répertoire : $repertoire_script"
read -p "Entrez le nom du dump (sans spécifier l'extension) que vous souhaitez restaurer (valeur par défaut: $nom_dump_default.sql) :" nom_dump
nom_dump=${nom_dump:-$nom_dump_default}
echo "Dump à restaurer : $nom_dump"

# Base de données C3PO
read -p "Entrez le nom de la version de la base de donnée C3PO à restaurer (valeur par défaut : $dbname_default): " dbname
dbname=${dbname:-$dbname_default}
echo "Nom de la base de données : $dbname"


# Spécification du chemin complet de restauration du dump
chemin_dump="$repertoire_script/$nom_dump.sql"

# Vérifier si la base de données du même nom existe
if PGPASSWORD="$mot_de_passe" psql -h "$hote" -p "$port" -U "$utilisateur" -lqt | cut -d \| -f 1 | grep -qw "$dbname"; then
	read -p "La base de données $dbname existe déjà. Voulez-vous la supprimer et restaurer le dump ? (Oui/Non): " reponse
    if [ "$reponse" = "Oui" ]; then
        # Supprimer la base de données "$dbname" existante
		PGPASSWORD="$mot_de_passe" dropdb -h "$hote" -p "$port" -U "$utilisateur" "$dbname"
		# Si une erreur se produit, afficher un message approprié
		if [ $? -eq 0 ]; then
			echo "Connexion à la base de données réussie."
		else
			echo "Erreur lors de la connexion à la base de données."
			echo "Veuillez vérifier les informations saisies et relancer le script."
			# Ajouter une pause pour empêcher la fermeture automatique
			read -p "Appuyez sur Entrée pour quitter..."
			exit 0
		fi
	else
		echo "Opération annulée. Fermeture de la console."
		# Ajouter une pause pour empêcher la fermeture automatique
		read -p "Appuyez sur Entrée pour quitter..."
		exit 0
	fi
fi

# Créer la base de données "$dbname"
PGPASSWORD="$mot_de_passe" createdb -h "$hote" -p "$port" -U "$utilisateur" -E UTF8 -T template0 "$dbname"

# Si une erreur se produit, afficher un message approprié
if [ $? -eq 0 ]; then
    echo "Création la base de données réussie."
else
    echo "Erreur lors de la création de la base de données."
	echo "Veuillez vérifier les informations saisies et relancer le script."
	# Ajouter une pause pour empêcher la fermeture automatique
	read -p "Appuyez sur Entrée pour quitter..."
	exit 0
fi

# Utiliser la commande psql pour restaurer le dump
PGPASSWORD="$mot_de_passe" psql -h "$hote" -p "$port" -U "$utilisateur" -d "$dbname" -f "$chemin_dump"

# Si une erreur se produit, afficher un message approprié
if [ $? -eq 0 ]; then
    echo "Restauration de la base de données réussie."
	# Ajouter une pause pour empêcher la fermeture automatique
	read -p "Appuyez sur Entrée pour quitter..."
else
    echo "Erreur lors de la restauration de la base de données."
	echo "Veuillez vérifier les informations saisies et relancer le script."
	# Ajouter une pause pour empêcher la fermeture automatique
	read -p "Appuyez sur Entrée pour quitter..."
fi
