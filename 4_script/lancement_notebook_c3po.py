
# -*- coding: utf-8 -*-
import asyncio
# Evite le warning RuntimeWarning:
asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
# Regle souci de certificat ssl sur atp_substance.ipynb (à valider pour les regles secu)
import ssl
ssl._create_default_https_context = ssl._create_unverified_context
import nbformat
from nbconvert.preprocessors import ExecutePreprocessor
import os
from pathlib import Path
from export_c3po_logging import create_logger
import configparser
from pathlib import Path
# Chemin vers le dossier script
dossier_script = Path(__file__).resolve()
print(f"Emplacement du script : {dossier_script}")
# Chemin vers le dossier de config
dossier_config = dossier_script.parent / 'config'
print(f"Emplacement du dossier de config : {dossier_config}")
# Fichier initialisant les variables d'environnement
fichier_ini = dossier_config / 'c3po_config.ini'
print(f"Emplacement du fichier d'initialisation : {fichier_ini}")
config = configparser.RawConfigParser()  # On crée un nouvel objet "config"
if not config.read(fichier_ini):
    raise FileNotFoundError(f"Le fichier {fichier_ini} n'a pas pu être trouvé ou chargé.")
config.read(fichier_ini)

# Param config
# notebookpath = config.get('variable', 'chemin_notebooks')
kernel_name = config.get('variable',  'kernel_name')

# Récuperer le path du dossier des notebooks (avec chemin relatif, autre façon de faire : chemin_notebook à spécifier dans le fichier .ini)
dossier_script = Path(__file__).resolve()
projetpath = dossier_script.parent.parent
print(projetpath)
notebookpath = str(projetpath / '1_notebook')

def demarre_logging():
    ''' charge le logging et creation du fichier de suivi'''
    log_dir = Path(__file__).parent / 'log'
    log_dir.mkdir(exist_ok=True)
    log_file = log_dir / 'lecture_notebook_c3po.log'
    return create_logger(log_file)

log = demarre_logging()

log.info('--------------debut script------------')

def recup_path_notebook(directory: Path):
    """Retourne une liste de chemins vers les notebooks .ipynb dans le répertoire spécifié."""
    notebook_paths = []
    for path in directory.rglob('*.ipynb'):
        if not any(excluded in path.parts for excluded in ['_OLD', '.ipynb_checkpoints', 'ATP']):
            notebook_paths.append(path)
    log.info(f'notebooks trouvés : {notebook_paths}')
    return notebook_paths

def run_notebook(liste_path : list[Path]):
    ''' boucle sur les notebooks pour lancement et enregistrement'''
    for notebook in liste_path:
        try:
            log.info(f'Début du traitement de {notebook}...')
            print(f'Début du traitement de {notebook}...')
            # répertoire du notebook
            notebook_dir = os.path.dirname(notebook)

            print (f"Repertoire de travail : {notebook_dir} ")
            # répertoire de travail
            os.chdir(notebook_dir)

            with open(notebook, 'r', encoding='utf-8') as f:
                #nb = nbformat.read(f, as_version=4)
                nb = nbformat.read(f,nbformat.NO_CONVERT)
                ep = ExecutePreprocessor(timeout=600, kernel_name=kernel_name)
                ep.preprocess(nb, {'metadata': {'path': notebook_dir}})

            print(f"Enregistrement du notebook {os.path.basename(notebook)}")
            with open(notebook, 'w', encoding='utf-8') as f:
                nbformat.write(nb, f)

            log.info(f"Fin du traitement de {notebook}.")
            print(f"Fin du traitement de {notebook}")
        except Exception as e:
            log.error(f"Erreur lors du traitement de {notebook}: {e}")
            print(f"Erreur lors du traitement de {notebook}: {e}")
    return

def clear_outputs(liste_path : list[Path]):
    """Efface les outputs de toutes les cellules de chaque notebook."""
    for notebook in liste_path:
        with open(notebook, 'r', encoding='utf-8') as f:
            nb = nbformat.read(f,nbformat.NO_CONVERT)
            
        # Parcours des cellules pour supprimer les outputs
        for cell in nb.cells:
            if 'outputs' in cell:
                cell['outputs'] = []
        
        # Sauvegarde du notebook avec les outputs effacés
        with open(notebook, 'w', encoding='utf-8') as f:
            nbformat.write(nb, f)
        print(f"Outputs effacés pour le notebook {notebook}.")

if __name__ == '__main__':
    #on convertit en path
    path_notebooks = Path(notebookpath)
    notebooks = recup_path_notebook(path_notebooks)
    run_notebook(notebooks)
    clear_outputs(notebooks)
    log.info('--------------fin de script------------')