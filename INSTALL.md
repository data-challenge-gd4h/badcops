# Guide d'installation

## I. Collecte des données

Le téléchargement des données sources est exécuté par les scripts Notebook Jupyter, à l'exception :
- du **référentiel des cultures du RPG**, disponible sur une [page dédiée de l'IGN](https://geoservices.ign.fr/documentation/donnees/vecteur/rpg) et dont les données doivent être intégrées manuellement dans l'onglet **'RPG_CULT_GPES_CULTS_{millésime}'**, puis les correspondances avec les cultures E-phy faites manuellement dans l'onglet **'ref_cult_rpg'** du [dictionnaire de données](https://gitlab.com/data-challenge-gd4h/c3po/-/blob/main/1_notebook/C3PO_dictionnaire_donnees_2025-02_V2.4.0.xlsx?ref_type=heads)

> **NB** <br>
> Les données sources sont acccessibles aux liens suivants :
> - **BNV-D** : <a href="https://www.data.gouv.fr/fr/datasets/ventes-de-pesticides-par-departement/" target="_blank" rel="noreferrer">Ventes</a> et <a href="https://www.data.gouv.fr/fr/datasets/achats-de-pesticides-par-code-postal/" target="_blank" rel="noreferrer">Achats</a>
> - <a href="https://www.data.gouv.fr/fr/datasets/donnees-ouvertes-du-catalogue-e-phy-des-produits-phytopharmaceutiques-matieres-fertilisantes-et-supports-de-culture-adjuvants-produits-mixtes-et-melanges/" target="_blank" rel="noreferrer">**E-phy**</a>
> - **RPG** : <a href="https://geoservices.ign.fr/sites/default/files/2023-02/REF_CULTURES_GROUPES_CULTURES_2021.csv"  target="_blank" rel="noreferrer">référentiel des cultures</a>
> - <a href="https://www.data.gouv.fr/fr/datasets/base-de-donnees-agritox/" target="_blank" rel="noreferrer">**Agritox**</a>
> - **ATP** : <a href="https://echa.europa.eu/fr/information-on-chemicals/annex-vi-to-clp" target="_blank" rel="noreferrer"> tableau d’entrées harmonisées disponible à l’annexe VI du règlement CLP</a>
> - <a href="https://ec.europa.eu/food/plant/pesticides/eu-pesticides-database/start/screen/active-substances" target="_blank" rel="noreferrer">**EU Pesticides Database**</a>
> - **Sandre** : référentiels <a href="https://api.sandre.eaufrance.fr/referentiels/v1/par.json?outputSchema=SANDREv4&filter=$StParametre=%27Valid%C3%A9%27%20and%20$ParametreChimique.CdCASSubstanceChimique!=%27%27" target="_blank" rel="noreferrer">Paramètres</a> et <a href="https://api.sandre.eaufrance.fr/referentiels/v1/gpr.json?outputSchema=SANDREv4&filter=$StGroupeParametres=%27Valid%C3%A9%27%20" target="_blank" rel="noreferrer">Groupe de Paramètres</a>

## II. Dépendances

Les programmes suivants sont nécessaires à l'éxecution du code :
- [LibreOffice Calc](https://fr.libreoffice.org/download/telecharger-libreoffice/)
- [Python](https://www.python.org/downloads/) (nécessite également [Jupyter](https://jupyter.org/install))
- [PostgreSQL](https://www.postgresql.org/)
- [GitBash](https://git-scm.com/downloads)

Pour installer les dépendances python nécessaires au fonctionnement du code, il est conseillé d'activer un environnement virtuel : 

```zsh
python -m venv env  # Créer l'environnement
# Pour Windows
.\env\Scripts\activate
# Pour macOS/Linux
source env/bin/activate
```

Lancez ensuite la commande suivante :

```zsh
pip install -r requirements.txt
```

Installez le kernel c3po :
```zsh
python -m ipykernel install --user --name=c3po --display-name="Python (c3po)"
```

Vérifiez que le kernel c3po a bien été installé :
```zsh
jupyter kernelspec list
```

## III. Développement

> **ATTENTION**<br>
>Afin de garantir la bonne exécution des scripts, on veillera à conserver l'arborescence du projet GitLab (dont une partie est décrite dans cette section).

### 0. Configuration des variables

Vérifier et ajuster si besoin les variables décrites dans le fichier [c3po_config.ini](https://gitlab.com/data-challenge-gd4h/c3po/-/blob/main/4_script/config/c3po_config.ini?ref_type=heads).

_Variables du fichier **c3po_config.ini** :_ 

```
[variable]
# Version de la base de données 
version = V1.3
# # Chemin du dossier contenant les notebook
# chemin_notebooks = D:\_gitlab\c3po\1_notebook
# Nom du kernel des Notebooks
kernel_name = c3po
# Choix de l'action pour le notebook 1.7_c3po/c3po_version.ipynb ('1' pour ajouter une ligne à la table c3po.version, '2' pour archiver et créer un nouveau fichier) 
choix_action = 1
# Url de téléchargement des données de la BNV-D
url_bnvd_ventes = https://data.ofb.fr/catalogue/Donnees-geographiques-OFB/api/records/bd45f801-45f7-4f8c-b128-a1af3ea2aa3e/attachments/BNVD_2023_VENTE.zip
url_bnvd_achats_1 = https://data.ofb.fr/catalogue/Donnees-geographiques-OFB/api/records/a69c8e76-13e1-4f87-9f9d-1705468b7221/attachments/BNVD_2023_ACHAT_2018-2022.zip
url_bnvd_achats_2 = https://data.ofb.fr/catalogue/Donnees-geographiques-OFB/api/records/a69c8e76-13e1-4f87-9f9d-1705468b7221/attachments/BNVD_2023_ACHAT_2013-2017.zip
url_bnvd_fonctions = https://backend-ventes-produits-phytopharmaceutiques.eaufrance.fr/tracabilite-api/a-propos/piece-jointe/20240718_referentiel_fonction_substances_V8.1.zip
date_publi_bnvd = 20231024 # A récupérer depuis 
```

> **ATTENTION**<br>
> N'oubliez notamment pas de vérifier à chaque installation la **date de publication (_date_publi_bnvd_)** et **les URL de téléchargement des données de la BNV-D**, ainsi que le **numéro de version de C3PO**.


### 1. Lancement des scripts Notebook Jupyter

Le script **[lancement_notebook_c3po.py](https://gitlab.com/data-challenge-gd4h/c3po/-/blob/main/4_script/lancement_notebook_c3po.py?ref_type=heads)** permet d'orchestrer l'exécution ordonnée des scripts Notebook Jupyter situés dans le répertoire [1_notebook](/1_notebook).

> **NB** <br>
> Le répertoire [1_notebook](/1_notebook) comporte autant de sous-répertoires qu'il y a de sources de données à intégrer dans C3PO, ainsi qu'un sous-répertoire [1_notebook/1.7_c3po](/1_notebook/1.7_c3po) pour générer les tables renseignant les métadonnées de la base.

#### 1.1. Prérequis : cas particulier des fichiers sources à intégrer manuellement

Lors de leur exécution, les scripts Notebook Jupyter téléchargent automatiquement les données en entrée dans le répertoire **/input** de chaque source de donnéees, à l'exception de la correspondance entre la nomenclature des cultures E-phy et le **référentiel des cultures du RPG**.

Le référentiel des cultures du RPG est disponible sur une [page dédiée de l'IGN](https://geoservices.ign.fr/documentation/donnees/vecteur/rpg) (voir lien vers la version 2021: https://geoservices.ign.fr/sites/default/files/2023-02/REF_CULTURES_GROUPES_CULTURES_2021.csv)

Ces données doivent être téléchargées puis intégrées manuellement dans l'onglet **'RPG_CULT_GPES_CULTS_{millésime}'**, puis les correspondances avec les cultures E-phy faites manuellement dans l'onglet **'ref_cult_rpg'** du [dictionnaire de données](https://gitlab.com/data-challenge-gd4h/c3po/-/blob/main/1_notebook/C3PO_dictionnaire_donnees_2023-12_V2.2.2.xlsx?ref_type=heads).

- Aperçu de l'onglet 'RPG_CULT_GPES_CULTS_{millésime}' du dictionnaire de données :
![Aperçu de l'onglet 'RPG_CULT_GPES_CULTS_{millésime}' du dictionnaire de données](image.png)

- Aperçu de l'onglet 'ref_cult_rpg' du dictionnaire de données :
![Aperçu de l'onglet 'ref_cult_rpg' du dictionnaire de données](image-1.png)

#### 1.2. Intégration des données sources (script [lancement_notebook_c3po.py](https://gitlab.com/data-challenge-gd4h/c3po/-/blob/main/4_script/lancement_notebook_c3po.py?ref_type=heads))

Lancer le  script **[lancement_notebook_c3po.py](https://gitlab.com/data-challenge-gd4h/c3po/-/blob/main/4_script/lancement_notebook_c3po.py?ref_type=heads)**.

```zsh
python .\4_script\lancement_notebook_c3po.py
```

##### Explication du script **lancement_notebook_c3po.py**

###### Intégration des données sources
Ce script commence par exécuter les fichiers .ipynb contenus dans les sous-répertoires relatifs à chaque source de données à intégrer dans C3PO, dans l'ordre indiqué par les numéros **1.X** en préfixe de leur nom "**1.X_{_source de données_}**". Ainsi, il commencera par exécuter les scripts de [1.1_bnvd](/1_notebook/1.1_bnvd), puis ceux de [1.2_ephy](/1_notebook/1.2_ephy), etc.

Les sous-répertoires par source de données sont organisés suivant une arboresence du type :
- **{_source de données_}\_substance.ipynb** -> Script Notebook Jupyter permettant de générer les fichiers .csv comportant des informations relatives aux substances actives, qui seront stockés dans le sous-répertoire **/output** et constitueront les tables intégrées dans chaque schéma de la base de données (à chaque source de donnée correspond un schéma).<br>
**_Sources de données concernées : toutes (BNV-D, E-phy, Agritox, ATP, EU Pesticides Database et Sandre)_**

> **NB** <br>
> Pour les sources de données **BNV-D** et **E-phy**, deux scripts Notebook Jupyter doivent être exécutés consécutivement, un nombre est ainsi ajouté en préfixe du nom de chaque script, afin de  spécifier que le script Notebook Jupyter à exécuter en premier pour ces sources de données est celui relatif aux substance (ex. **_1_bnvd_substance.ipynb_**).

- **{_source de données_}\_amm.ipynb** _(optionnel)_ -> Script Notebook Jupyter permettant de générer les fichiers .csv comportant des informations relatives aux produits, qui seront stockés dans le sous-répertoire **/output**.<br>
**_Sources de données concernées : BNV-D et E-phy_**

> **NB** <br>
> Pour les sources de données **BNV-D** et **E-phy**, deux scripts Notebook Jupyter doivent être exécutés consécutivement, un nombre est ainsi ajouté en préfixe du nom de chaque script, afin de  spécifier que le script Notebook Jupyter à exécuter en second pour ces sources de données est celui relatif aux produits (ex. **_2_bnvd_amm.ipynb_**).

- **/input** -> sous-répertoire où sont stockées les données sources nécessaires à la poursuite d'exécution du script. (voir **[cas particulier de la EU Pesticides Database](#eu-pesticides-database--fichier-%C3%A0-d%C3%A9poser-dans-le-sous-r%C3%A9pertoire-input)**)<br>
- **/interm** _(optionnel)_ -> sous-répertoire où sont stockées des données intermédiaires exportées en cours de script.<br>
**_Sources de données concernées : BNV-D, E-phy, Agritox et ATP_**
- **/output** -> sous-répertoire où sont stockées les données en sortie des scripts Notebook Jupyter (= tables qui seront ensuite intégrées dans la base de données C3PO).

###### Finalisation des métadonnées de C3PO
Une fois tous les scripts Notebook Jupyter des sources de données exécutés avec succès, **lancement_notebook_c3po.py** exécute le script [1_notebook/1.7_c3po](/1_notebook/1.7_c3po/c3po_version.ipynb). Ce dernier permet de générer/finaliser les tables `version` et `source` renseignant les métadonnées de la base.

###### Stockage des logs
Enfin, tout au long de son exécution, **lancement_notebook_c3po.py** stocke les logs dans un dossier dédié (4_script/log) en faisant appel à [export_c3po_logging.py](https://gitlab.com/data-challenge-gd4h/c3po/-/blob/main/4_script/export_c3po_logging.py?ref_type=heads).


### 2. Génération de la base de données C3PO

#### Prérequis
Avant de suivre les instructions suivantes, assurez-vous d’avoir exécuté avec succès les scripts Notebook Jupyter du répertoire **[1_notebook](/1_notebook)** conformément aux [instructions précédentes](#1-lancement-des-scripts-notebook-jupyter) et de disposer des fichiers .csv en sortie de ces scripts dans les sous-répertoires **/1_notebook/1.X_{_source de données_}/output** (X : numéro propre à chaque source de données) et **/1_notebook/1.7_c3po** (pour les tables `version` et `source` renseignant les métadonnées de la base).

Il existe ensuite deux possibilités pour générer la base de données c3po à partir des outputs :

#### Installation de la base de données c3po à partir des fichiers générés par les scripts Notebook Jupyter en passant par bash

**1.** Lancez le serveur PostgreSQL dans le CLI

Sur Linux (Ubuntu, CentOS 7+, etc.) :
```zsh
sudo systemctl start postgresql
```
Sur d'anciennes distributions Linux ou sur wsl :
```zsh
sudo service start postgresql
```
Sur MacOS :
```zsh
brew services start postgresql
```
Sur Windows :
```zsh
net start postgresql
```

Pensez à relever vos informations de connexion à PostGreSQL (utilisateur, mot de passe) ainsi votre hôte et votre port de connexion.

**2.** Lancez le script bash

```zsh
cd 2_bdd/2.1_creation_bdd/
bash c3po_create_db_v1.sh
```

**3.** Vérifiez que vos tables ont bien été créées dans la base de données c3po

- Vérifiez que la base de données a bien été créée (exemple ci-dessous avec recherche de toutes les bases de données comportant "c3po" dans leur nom, remplacer <nom_utilisateur> par la valeur adéquate, par exemple "postgres").
Sur Windows :
```zsh
psql -U <nom_utilisateur> -d postgres -Atc "SELECT datname FROM pg_database WHERE datname LIKE '%c3po%';"
```
- Puis, affichez le nombre d'enregistrements dans chaque table (remplacer <nom_utilisateur> et <nom_de_la_bdd> par les valeurs adéquates, par exemple "postgres" et "c3po_v1").
Sur Windows :
```zsh
psql -U <nom_utilisateur> -d <nom_de_la_bdd> -Atc "SELECT schemaname ||'.'|| relname ||' : '|| n_live_tup ||' lignes' FROM pg_stat_user_tables WHERE schemaname IN ('bnvd','ephy','agritox','atp','eupdb','sandre','c3po') ORDER BY schemaname, relname;"
```

#### Installation de la base de données c3po à partir d’un dump

##### Création d’un dump SQL à partir de la base existante

**1.** Lancez le script **[c3po_create_dump_db_v1.sh](/2_bdd/2.2_dump_bdd/c3po_create_dump_db_v1.sh)** (répertoire [2_bdd/2.2_dump_bdd](/2_bdd/2.2_dump_bdd)).

```zsh
cd ../2.2_dump_bdd
bash c3po_create_dump_db_v1.sh
```

**2.** Renseignez les informations demandées par la console.

**3.** Vérifiez que le dump a été correctement créé.

##### Restauration d’un dump SQL

> **ATTENTION**<br>
> Pour que ce processus fonctionne, il est impératif de disposer d’un fichier dump de la base c3po au format .sql  dans le répertoire [2_bdd/2.2_dump_bdd](/2_bdd/2.2_dump_bdd)

**1.** Lancez le script **[c3po_restore_dump_db_v1.sh](/2_bdd/2.2_dump_bdd/c3po_restore_dump_db_v1.sh)** (répertoire [2_bdd/2.2_dump_bdd](/2_bdd/2.2_dump_bdd)).

```zsh
cd 2_bdd/2.2_dump_bdd/
bash c3po_restore_dump_db_v1.sh
```

**2.** Renseignez les informations demandées par la console.

**3.** Vérifiez que le dump a été correctement restauré.

## Production

La solution C3PO n'est pas encore prête pour un déploiement en production.
